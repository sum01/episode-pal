#include "commandable.hpp"

void ep::commandable::redo_last() noexcept {
	this->commands.redo();
}

void ep::commandable::undo_last() noexcept {
	this->commands.undo();
}
