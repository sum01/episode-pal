#include "media.hpp"

#include <string_view>
#include <locale>

// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>

static std::string handle_string(const std::string_view str, const bool first_caps) noexcept {
	std::string res = str.data();
	if (first_caps) {
		// Replace the first char with an upper-cased version of itself.
		// Uses the system locale for locale-specific capitilaztion.
		res.front() = std::toupper(res.front(), std::locale());
	}
	return res;
}

std::string ep::media::to_string(const ep::media::type &mtype, const bool first_caps) noexcept {
	switch (mtype) {
		case ep::media::type::MANGA:
			return handle_string(gettext("manga"), first_caps);
		case ep::media::type::TV:
			return handle_string(gettext("TV"), first_caps);
		case ep::media::type::MOVIE:
			return handle_string(gettext("movie"), first_caps);
		case ep::media::type::ANIME:
			[[fallthrough]];
		default:
			return handle_string(gettext("anime"), first_caps);
	}
}

std::string ep::media::to_string(const ep::media::status &mstatus, const bool first_caps) noexcept {
	switch (mstatus) {
		case ep::media::status::ABANDONED:
			return handle_string(gettext("abandoned"), first_caps);
		case ep::media::status::FINISHED:
			return handle_string(gettext("finished"), first_caps);
		case ep::media::status::UNFINISHED:
			[[fallthrough]];
		default:
			return handle_string(gettext("unfinished"), first_caps);
	}
}

/* enum class data_i : int {NAME = 0, ALT_NAME, TYPE, STAT, */
/* 													 LAST_V, LAST_EC, MAX_V, MAX_EC, */
/* 													 NOTES, RATING, DESC, REMOTE}; */
ep::media::data_i &ep::media::operator++(ep::media::data_i &d) noexcept {
	switch (d) {
		case data_i::NAME:
			return d = data_i::ALT_NAME;
		case data_i::ALT_NAME:
			return d = data_i::TYPE;
		case data_i::TYPE:
			return d = data_i::STAT;
		case data_i::STAT:
			return d = data_i::LAST_V_S;
		case data_i::LAST_V_S:
			return d = data_i::LAST_EC;
		case data_i::LAST_EC:
			return d = data_i::MAX_V_S;
		case data_i::MAX_V_S:
			return d = data_i::MAX_EC;
		case data_i::MAX_EC:
			return d = data_i::NOTES;
		case data_i::NOTES:
			return d = data_i::RATING;
		case data_i::RATING:
			return d = data_i::DESC;
		case data_i::DESC:
			return d = data_i::REMOTE;
		default:
			return d;
	}
}
