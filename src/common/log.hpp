#ifndef EP_COMMON_LOG_HPP
#define EP_COMMON_LOG_HPP

#include <string_view>

namespace ep {
	// Logs a string to a temp file if built as release.
	// Prints to cerr if built as debug and appends a newline.
	void log(const std::string_view) noexcept;
}

#endif
