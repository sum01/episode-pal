#include "file_reader.hpp"

#include <fstream>

std::optional<std::string> ep::read_file(const std::filesystem::path &path) noexcept {
	// Only bother trying to read the config if it exists
	if (!std::filesystem::exists(path)) {
		return std::nullopt;
	}
	// Open the file in read-only mode.
	std::ifstream conf_reader {path, std::ios_base::in};

	if (!conf_reader.is_open()) {
		return std::nullopt;
	}

	std::string line, results;
	while (std::getline(conf_reader, line)) {
		results += line;
	}

	return results;
}
