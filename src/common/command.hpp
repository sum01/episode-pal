#ifndef EP_COMMAND_HPP
#define EP_COMMAND_HPP

namespace ep {
	// Generic command structure for undo/redo
	class command {
public:
		virtual void execute() noexcept = 0;
		virtual void undo() noexcept = 0;
	};
}

#endif
