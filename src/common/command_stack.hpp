#ifndef EP_COMMAND_STACK_HPP
#define EP_COMMAND_STACK_HPP

#include "command.hpp"

#include <stack>
#include <memory>

namespace ep {
	// Defines a simple undo & redo handler-type data structure.
	// Make sure to push all commands to it.
	class command_stack {
public:
		// Saves the command and executes it.
		// Removes any previous redo history as well.
		void execute(std::unique_ptr<ep::command> ) noexcept;
		// Trigger execute on the last stored command.
		// Moves that command into the executed stack.
		void redo() noexcept;
		// Trigger undo on the last previously executed command.
		// Moves that command into the stored stack.
		void undo() noexcept;
		// Clears both command stacks.
		void clear_command_history() noexcept;
private:
		// Deque lets us remove back elements if they outgrow our max history.
		std::stack<std::unique_ptr<ep::command> > undo_stack,
																							redo_stack;
	};
}

#endif
