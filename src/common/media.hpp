#ifndef EP_COMMON_MEDIA_HPP
#define EP_COMMON_MEDIA_HPP

#include <optional>
#include <string>

// TODO: make to_string calls constexpr when it gets compiler support
namespace ep::media {
	// Identifiers for specific types of data we handle
	enum class type {ANIME, MANGA, TV, MOVIE};
	// Different status types for shows/whatever.
	// Can be used to sort data & whatnot
	enum class status {UNFINISHED, FINISHED, ABANDONED};
	// Converts the enum to a literal string. Accepts an optional bool to return the first letter capitalized
	// Note that gettext is already used in the implementation, so there's no need to run it against the result.
	[[nodiscard]]
	std::string to_string(const ep::media::type &, const bool = false) noexcept;

	// Converts the enum to a literal string. Accepts an optional bool to return the first letter capitalized
	// Note that gettext is already used in the implementation, so there's no need to run it against the result.
	[[nodiscard]]
	std::string to_string(const ep::media::status &, const bool = false) noexcept;

	// A simple way to pass the data about a show/manga/anime/whatever around.
	// Using a template because we sometimes want these named members with a different type.
	// Parameters will default-initialize to the first type if not set.
	template<typename T>
	struct main_data_t {
		// Default-initialize lets us do aggregate initialization without addressing
		T name = T{},
			alt_name = T{},
			type = T{},
			status = T{},
			last_volume_season = T{},
			last_episode_chapter = T{},
			max_volume_season = T{},
			max_episode_chapter = T{},
			notes = T{},
			rating = T{},
			description = T{},
			remote_id = T{};
	};

	// This is main_data_t<T> with the added ep::media::type field for when it's needed.
	template<typename T>
	struct data_t : public main_data_t<T> {
		// An internal non-templated type we'll use sometimes
		ep::media::type internal_type = ep::media::type::ANIME;
	};

	// A common type we'll use all over the place.
	using data = data_t<std::optional<std::string> >;

	enum class data_i : int {NAME = 0, ALT_NAME, TYPE, STAT,
													 LAST_V_S, LAST_EC, MAX_V_S, MAX_EC,
													 NOTES, RATING, DESC, REMOTE};

	// Increment a data_i object, or does nothing if it's at the end.
	data_i &operator++(data_i &d) noexcept;
}

#endif
