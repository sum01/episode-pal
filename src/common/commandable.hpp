#ifndef EP_COMMANDABLE_HPP
#define EP_COMMANDABLE_HPP

#include "command_stack.hpp"

namespace ep {
	// Something that any class that can handle do/undo should inherit.
	class commandable {
public:
		virtual void redo_last() noexcept;
		virtual void undo_last() noexcept;
protected:
		ep::command_stack commands;
	};
}

#endif
