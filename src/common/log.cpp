#include "log.hpp"

#include <iostream>
// For g_log_structured
#include <glib-2.0/glib.h>

void ep::log(const std::string_view str) noexcept {
	// Logs a message
	g_message(str.data());
}
