#ifndef EP_DIRS_HPP
#define EP_DIRS_HPP

#include <string>

// TODO: make these constexpr when constexpr std::string is implemented in GCC
namespace ep {
	// Gets the installed location of the glade files.
	// Note: will be automatically editted upon Cmake install to point to the install location.
	[[nodiscard]]
	std::string get_installed_data_dir() noexcept;
	// This is the preferred way to search for the data dir.
	// You should only use get_installed_data_dir if you tried get_xdg_data_dir and it failed.
	// Possible Linux path: ~/.local/share/episode_pal
	[[nodiscard]]
	std::string get_xdg_data_dir() noexcept;

	// The standard path to the XDG config dir with our program name postfixed onto it.
	// Possible Linux path: ~/.config/episode_pal
	[[nodiscard]]
	std::string get_xdg_config_dir() noexcept;
}

#endif
