#ifndef EP_JSON_HPP
#define EP_JSON_HPP

#include <string>
#include <string_view>

#include <nlohmann/json.hpp>

// TODO: make all this stuff constexpr when std::string constexpr is supported
namespace ep::json {
	namespace detail {
		template<typename T>
		std::string to_string(const T &v) noexcept {
			return std::to_string(v);
		}

		template<>
		inline std::string to_string<std::string>(const std::string &v) noexcept {
			return v;
		}

		template<>
		inline std::string to_string<std::string_view>(const std::string_view &v) noexcept {
			return v.data();
		}
	}
	// This either gets the result, or if it's null, returns a default-constructed type T
	// Doesn't convert to a string
	template<typename T>
	[[nodiscard]]
	T safe_get_t(const nlohmann::json &json) noexcept {
		// Since fields can be "NULL" sometimes, we need to be safe when getting the results
		return json.is_null() ? T{} : json.get<T>();
	}

	// This either gets the result, or if it's null, returns a default-constructed type T
	// Converts the result to a string if it isn't already.
	// nlohmann::basic_json::get() isn't constexpr
	template<typename T>
	[[nodiscard]]
	std::string safe_get(const nlohmann::json &json) noexcept {
		// Since fields can be "NULL" sometimes, we need to be safe when getting the results
		return detail::to_string<T>(safe_get_t<T>(json));
	}
}

#endif
