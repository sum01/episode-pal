#include "formatting.hpp"

// Needed for std::isspace
#include <cctype>
#include <regex>

std::string ep::remove_end_space(const std::string_view str) noexcept {
	if (str.empty()) {
		return {};
	}

	std::string_view::size_type n_char_delete = 0;
	// Go through the string char-by-char backwards
	for (auto r_itr = str.crbegin(); r_itr != str.crend(); ++r_itr) {
		// Stop when we hit a non-space char, since..
		// that's the end of what we care about
		if (!std::isspace(*r_itr)) {
			break;
		}
		++n_char_delete;
	}

	// Create our copy of the passed string without the end (empty) chars
	return std::string{str.substr(0, str.size() - n_char_delete)};
}

std::string ep::process_description(std::string description) noexcept {
	// The description has stuff like <br>, \r\n, \"blah blah\", and (Source: Funimation) in it.
	// So we remove html tags
	// any \r\n
	// Quote escapes for those inner strings
	// things like [Written by MAL Rewrite]
	// TODO: Empty newlines (only) at the end
	// and the whole (Source: Whatever) thing
	const std::regex junk_regex {R"#(<\/?[^>]+>|\(Source:[^)]+\)|\\r|\\n|\\|\[Written by[^\]]+\])#"};

	// Replace all matches with an empty string
	return std::regex_replace(description, junk_regex, "");
}
