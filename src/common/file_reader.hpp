#ifndef EP_FILE_READER_HPP
#define EP_FILE_READER_HPP

#include <string>
#include <optional>
#include <filesystem>

namespace ep {
	// Just read a file into a string (if possible). If it fails, the optional is nullopt.
	std::optional<std::string> read_file(const std::filesystem::path &) noexcept;
}

#endif
