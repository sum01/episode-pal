#ifndef EP_FORMATTING_HPP
#define EP_FORMATTING_HPP

#include <string>
#include <string_view>

// TODO: make constexpr when std::string constexpr is supported by GCC
namespace ep {
	// Removes the empty newlines/spaces at the end of a given string.
	// Will NOT remove any lines between paragraphs.
	[[nodiscard]]
	std::string remove_end_space(const std::string_view) noexcept;

	// This function removes junk from the given string, and returns the fixed description.
	// Doesn't cleanup any excess space at the end. If you need that, use remove_end_space afterwards.
	[[nodiscard]]
	std::string process_description(std::string description) noexcept;
}

#endif
