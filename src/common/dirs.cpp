#include "dirs.hpp"

#include <glibmm-2.4/glibmm/miscutils.h>
// Used to get the correct seperator for the OS
#include <filesystem>
#include <string_view>

// Will be filled with the proper name from Cmake
// TODO: Just make a std::string when it's constexpr
static constinit std::string_view program_name = "@PROJECT_NAME@";

// TODO: do this stuff with std::string once constexpr std::string is suppoted in GCC
std::string ep::get_xdg_data_dir() noexcept {
	// The stuff between the at signs will be replaced by Cmake at build-time
	return Glib::get_user_data_dir() + std::filesystem::path::preferred_separator + program_name.data();
}

// TODO: do this stuff with std::string once constexpr std::string is suppoted in GCC
std::string ep::get_installed_data_dir() noexcept {
	// The stuff between the at signs will be replaced by Cmake at build-time
	return std::string {"@CMAKE_INSTALL_FULL_DATADIR@"} + std::filesystem::path::preferred_separator + program_name.data();
}

std::string ep::get_xdg_config_dir() noexcept {
	return Glib::get_user_config_dir() + std::filesystem::path::preferred_separator + program_name.data();
}
