#include "command_stack.hpp"

// for std::move
#include <utility>

void ep::command_stack::undo() noexcept {
	if (!this->redo_stack.empty()) {
		this->redo_stack.top()->undo();
		// Pass the object to our stored stack
		this->undo_stack.emplace(this->redo_stack.top().release());
		// Remove the now-empty pointer.
		this->redo_stack.pop();
	}
}

void ep::command_stack::redo() noexcept {
	if (!this->undo_stack.empty()) {
		this->undo_stack.top()->execute();
		// Pass the object to our executed stack
		this->redo_stack.emplace(this->undo_stack.top().release());
		// Remove the now-empty pointer.
		this->undo_stack.pop();
	}
}

void ep::command_stack::execute(std::unique_ptr<ep::command> c) noexcept {
	// Clear the undo history whenever new commands are pushed.
	while (!this->undo_stack.empty()) {
		this->undo_stack.pop();
	}
	// Save the command and run it.
	this->redo_stack.emplace(std::move(c));
	this->redo_stack.top()->execute();
}

void ep::command_stack::clear_command_history() noexcept {
	bool undo_empty = this->undo_stack.empty(),
			 redo_empty = this->redo_stack.empty();

	// Clearing both stacks in a single loop prevents needing to loop twice.
	// Although it adds the overhead of two bools and 3 ifs, which is probably minor in comparison..
	while (true) {
		if (!undo_empty) {
			this->undo_stack.pop();
			undo_empty = this->undo_stack.empty();
		}
		if (!redo_empty) {
			this->redo_stack.pop();
			redo_empty = this->redo_stack.empty();
		}
		if (undo_empty && redo_empty) {
			break;
		}
	}
}
