char *s = N_("Unfinished");
char *s = N_("Finished");
char *s = N_("Abandoned");
char *s = N_("Anime");
char *s = N_("Manga");
char *s = N_("TV");
char *s = N_("Movie");
char *s = N_("Name");
char *s = N_("Alt. Name");
char *s = N_("Type");
char *s = N_("Status");
char *s = N_("Last vol./season");
char *s = N_("Last ep./chap.");
char *s = N_("Max vol./season");
char *s = N_("Max ep./chap.");
char *s = N_("Notes");
char *s = N_("Rating");
char *s = N_("Description");
char *s = N_("anime");
char *s = N_("tv");
char *s = N_("manga");
char *s = N_("movie");
/* Placeholder text in the search bar */
char *s = N_("enter something to search");
char *s = N_("Display Type");
/* The tooltip for hovering over the display type dropdown box. */
char *s = N_("Controls what content will be displayed.");
char *s = N_("All");
char *s = N_("Anime only");
char *s = N_("Manga only");
char *s = N_("TV only");
char *s = N_("Movies only");
char *s = N_("Try to update the \"max\" fields on the selected row");
char *s = N_("Make a new row");
char *s = N_("_File");
char *s = N_("Make a new, empty save file ");
char *s = N_("Open an existing save file");
char *s = N_("Save this file to its last location");
char *s = N_("Save this file to a new location");
char *s = N_("_Edit");
char *s = N_("Delete a row in the file");
char *s = N_("Hides the description column from the display.");
char *s = N_("Hide description");
char *s = N_("Change the program settings");
char *s = N_("_Help");
char *s = N_("Some information on using this program");
char *s = N_("button");
