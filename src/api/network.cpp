#include "network.hpp"
#include "common/dirs.hpp"

// Used for throwing std::runtime_error
#include <stdexcept>
#include <memory>

// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>
// Used for making HTTPS requests
#include <httplib.h>

nlohmann::json ep::api::network::get_data(const ep::api::request &request) {
	// Note this url shouldn't have https:// in front of it...
	// Just a quirk with httplib
	httplib::SSLClient client(request.url);

	// Just using 5 second timeout for now. Maybe adjust later..
	client.set_connection_timeout(5);
	// Tells it to use data compression (requires ZLIB)
	client.set_compress(request.use_compression);
	// Make sure we check their HTTPS cert is valid.
	// This will use the system cert file.
	client.enable_server_certificate_verification(true);

	// Might not really need headers, but doing it anyways...
	/* const httplib::Headers headers = { */
	/* 	{"Content-Type", request.type}, */
	/* 	/1* {"Content-Encoding", "gzip"}, *1/ */
	/* 	/1* {"Accept-Encoding", "gzip, deflate"}, *1/ */
	/* 	{"Accept", "application/json"} */
	/* }; */

	// cpp-httplib seems to require we pass C-strings for the type
	// NOTE: Assuming all data will be JSON, might need to change in the future...
	const std::shared_ptr<httplib::Response> response = request.use_post ?
																											client.Post(request.endpoint.c_str(), request.body, request.type.c_str()) :
																											client.Get(request.endpoint.c_str());

	// Need to error-check the results, in case there was some issue
	// This could be timeouts, bad requests, etc.
	if (!response) {
		throw std::runtime_error(gettext("No response"));
	} else if (response->body.empty()) {
		throw std::runtime_error(gettext("Empty response"));
	}

	// This can also throw if the parsing fails
	// But since this method isn't noexcept, just return it
	return nlohmann::json::parse(response->body);
}
