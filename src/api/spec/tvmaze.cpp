#include "tvmaze.hpp"

#include "common/formatting.hpp"
#include "common/json.hpp"
#include "common/log.hpp"

// For string, to_string, and stoi
#include <string>
// For std::runtime_error
#include <stdexcept>

// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>

// FIXME: <format> isn't implemented in any compilers yet
// #include <format>

// NOTES:
// - endDate can be used (from seasons) to determine if a season is done broadcasting

static ep::api::request form_request(const std::string &endpoint) noexcept {
	return {
		// Not putting https:// here because cpp-httplib seems to require not using it.
		.url = "api.tvmaze.com",
		// Fuzzy-finds shows
		// Some example call: http://api.tvmaze.com/search/shows?q=dancing
		// This endpoint doesn't allow embed to be used so no season/episode info :(
		.endpoint = endpoint,
		// Body isn't used for GET
		.body = "",
		// No type since there's no body
		.type = "",
		// tvmaze uses GET
		.use_post = false,
		// Not sure if they allow compression or not...
		.use_compression = true
	};
}

ep::api::request ep::api::spec::tvmaze::get_request(const std::string_view name) noexcept {
	// FIXME: <format> isn't implemented in any compilers yet.
	// But this should work once it's implemented...
	// return std::format("\{ \"query\": \"{}\", \"variables\": \"{}\"\}\}", query.data(), search_name.data());
	return form_request(std::string {"/search/shows?q="} + name.data());
}

ep::api::request ep::api::spec::tvmaze::get_request(const ep::media::data &pre_data) {
	// Safety-check the passed data
	if (!pre_data.remote_id) {
		throw std::runtime_error(gettext("TVMaze was asked to for a request using data without any ID!"));
	}
	// FIXME: <format> isn't implemented in any compilers yet.
	// But this should work once it's implemented...
	// return std::format("\{ \"query\": \"{}\", \"variables\": \"{}\"\}\}", query.data(), search_name.data());

	// embed lets us ask for additional data with the same request.
	// So we're asking for both the shows (known about) seasons & episodes.
	return form_request(std::string {"/shows/"} + *pre_data.remote_id + "?embed[]=seasons&embed[]=episodes");
}

ep::media::data ep::api::spec::tvmaze::get_data(const nlohmann::json &json_obj) {
	// Safety-check the passed JSON
	if (!json_obj.is_object()) {
		throw std::runtime_error(gettext("TVMaze was given invalid JSON (not an object)."));
	}

	/* Example results in a json_obj
			{
				"score": 15.928811,
				"show": {
					"id": 534,
					"url": "http://www.tvmaze.com/shows/534/pawn-stars",
					"name": "Pawn Stars",
					"type": "Reality",
					"language": "English",
					"genres": ["History"],
					"status": "Running",
					"runtime": 60,
					"premiered": "2009-07-19",
					"officialSite": "http://www.history.com/shows/pawn-stars",
					"schedule": { "time": "22:00", "days": ["Friday"] },
					"rating": { "average": 8 },
					"weight": 97,
					"network": {
						"id": 53,
						"name": "History",
						"country": {
							"name": "United States",
							"code": "US",
							"timezone": "America/New_York"
						}
					},
					"webChannel": null,
					"externals": { "tvrage": 23281, "thetvdb": 111051, "imdb": "tt1492088" },
					"image": {
						"medium": "http://static.tvmaze.com/uploads/images/medium_portrait/171/429547.jpg",
						"original": "http://static.tvmaze.com/uploads/images/original_untouched/171/429547.jpg"
					},
					"summary": "<p>Long before banks, ATMS and check-cashing services, there were pawn shops. Pawning was the leading form of consumer credit in the United States until the 1950s, and pawn shops are still helping everyday people make ends meet. <b>Pawn Stars</b> takes you inside the colorful world of the pawn business. At the Gold &amp; Silver Pawn Shop on the outskirts of Las Vegas, three generations of the Harrison family - grandfather Richard, son Rick and grandson Corey - jointly run the family business, and there's clashing and camaraderie every step of the way. The three men use their sharp eyes and skills to assess the value of items from the commonplace to the truly historic, including a 16th-century samurai sword, a Super Bowl ring, a Picasso painting and a 17th-century stay of execution. It's up to them to determine what's real and what's fake, as they reveal the often surprising answer to the questions on everyone's mind, \"What's the story behind it?\" and \"What's it worth?\"</p>",
					"updated": 1594090743,
					"_links": {
						"self": { "href": "http://api.tvmaze.com/shows/534" },
						"previousepisode": { "href": "http://api.tvmaze.com/episodes/1887538" },
						"nextepisode": { "href": "http://api.tvmaze.com/episodes/1887539" }
					}
				}
			}
		*/
	const auto &media_object = json_obj.at("show");
	ep::media::data params;
	params.name = ep::json::safe_get<std::string>(media_object.at("name")),
	params.type = ep::media::to_string(ep::media::type::TV),
	// Doesn't need to remove end space, as they don't leave excess garbage in their summary's.
	params.description = ep::process_description(ep::json::safe_get<std::string>(media_object.at("summary"))),
	params.remote_id = ep::json::safe_get<int>(media_object.at("id")),
	params.internal_type = ep::media::type::TV;

	return params;
}

std::vector<ep::media::data> ep::api::spec::tvmaze::get_top_data(const nlohmann::json &json) noexcept {
	std::vector<ep::media::data> data_res;

	// The TVMaze results are all inside a 1-sized array
	// But doing the for-loop like so seems to tell Nlohmann-json to go through each JSON object inside it.
	// So whatever...
	for (const auto &json_obj : json) {
		try {
			data_res.emplace_back(ep::api::spec::tvmaze::get_data(json_obj));
		} catch (const std::exception &e) {
			ep::log(e.what());
			continue;
		}
	}

	return data_res;
}

void ep::api::spec::tvmaze::update_data(const nlohmann::json &json_obj, ep::media::data &data_to_update) {
	/* Example results
	{
	"id": 1,
	"url": "http://www.tvmaze.com/shows/1/under-the-dome",
	"name": "Under the Dome",
	"type": "Scripted",
	"language": "English",
	"genres": ["Drama", "Science-Fiction", "Thriller"],
	"status": "Ended",
	"runtime": 60,
	"premiered": "2013-06-24",
	"officialSite": "http://www.cbs.com/shows/under-the-dome/",
	"schedule": { "time": "22:00", "days": ["Thursday"] },
	"rating": { "average": 6.5 },
	"weight": 97,
	"network": {
		"id": 2,
		"name": "CBS",
		"country": {
			"name": "United States",
			"code": "US",
			"timezone": "America/New_York"
		}
	},
	"webChannel": null,
	"externals": { "tvrage": 25988, "thetvdb": 264492, "imdb": "tt1553656" },
	"image": {
		"medium": "http://static.tvmaze.com/uploads/images/medium_portrait/81/202627.jpg",
		"original": "http://static.tvmaze.com/uploads/images/original_untouched/81/202627.jpg"
	},
	"summary": "<p><b>Under the Dome</b> is the story of a small town that is suddenly and inexplicably sealed off from the rest of the world by an enormous transparent dome. The town's inhabitants must deal with surviving the post-apocalyptic conditions while searching for answers about the dome, where it came from and if and when it will go away.</p>",
	"updated": 1573667713,
	"_links": {
		"self": { "href": "http://api.tvmaze.com/shows/1" },
		"previousepisode": { "href": "http://api.tvmaze.com/episodes/185054" }
	},
	"_embedded": {
		"seasons": [
			{
				"id": 1,
				"url": "http://www.tvmaze.com/seasons/1/under-the-dome-season-1",
				"number": 1,
				"name": "",
				"episodeOrder": 13,
				"premiereDate": "2013-06-24",
				"endDate": "2013-09-16",
				"network": {
					"id": 2,
					"name": "CBS",
					"country": {
						"name": "United States",
						"code": "US",
						"timezone": "America/New_York"
					}
				},
				"webChannel": null,
				"image": {
					"medium": "http://static.tvmaze.com/uploads/images/medium_portrait/24/60941.jpg",
					"original": "http://static.tvmaze.com/uploads/images/original_untouched/24/60941.jpg"
				},
				"summary": "",
				"_links": { "self": { "href": "http://api.tvmaze.com/seasons/1" } }
			},
			{
				"id": 2,
				"url": "http://www.tvmaze.com/seasons/2/under-the-dome-season-2",
				"number": 2,
				"name": "",
				"episodeOrder": 13,
				"premiereDate": "2014-06-30",
				"endDate": "2014-09-22",
				"network": {
					"id": 2,
					"name": "CBS",
					"country": {
						"name": "United States",
						"code": "US",
						"timezone": "America/New_York"
					}
				},
				"webChannel": null,
				"image": {
					"medium": "http://static.tvmaze.com/uploads/images/medium_portrait/24/60942.jpg",
					"original": "http://static.tvmaze.com/uploads/images/original_untouched/24/60942.jpg"
				},
				"summary": "",
				"_links": { "self": { "href": "http://api.tvmaze.com/seasons/2" } }
			},
			{
				"id": 6233,
				"url": "http://www.tvmaze.com/seasons/6233/under-the-dome-season-3",
				"number": 3,
				"name": "",
				"episodeOrder": 13,
				"premiereDate": "2015-06-25",
				"endDate": "2015-09-10",
				"network": {
					"id": 2,
					"name": "CBS",
					"country": {
						"name": "United States",
						"code": "US",
						"timezone": "America/New_York"
					}
				},
				"webChannel": null,
				"image": {
					"medium": "http://static.tvmaze.com/uploads/images/medium_portrait/182/457332.jpg",
					"original": "http://static.tvmaze.com/uploads/images/original_untouched/182/457332.jpg"
				},
				"summary": "",
				"_links": { "self": { "href": "http://api.tvmaze.com/seasons/6233" } }
			}
		]
	}
	}
	 */
	// Safety-check the passed JSON
	if (!json_obj.is_object()) {
		throw std::runtime_error(gettext("TVMaze was given invalid JSON (not an object)."));
	}
	// The embedded data on this show is an array of seasons & their info.
	// This embedded info contains the respective maximum episode count as well.
	int max_season = 0,
			max_episode = 0;
	for (const auto &season_object : json_obj.at("_embedded").at("seasons")) {
		const int season = ep::json::safe_get_t<int>(season_object.at("number"));
		if (season > max_season) {
			max_season = season;
			// Always save max_episode as this seasons episode
			max_episode = ep::json::safe_get_t<int>(season_object.at("episodeOrder"));
		}
	}
	const std::string max_season_str = std::to_string(max_season);
	// Screw it, if there's no max season data just fill it with the first result.
	// Or if it's bigger than what's already stored, do that too.
	if (!data_to_update.max_volume_season || max_season > std::stoi(*data_to_update.max_volume_season)) {
		data_to_update.max_volume_season = max_season_str;
		// We only want to overwrite the episode data if the seasons match
	}
	// Not an else-if because if we just filled the season, we want to also update the max episodes
	if (max_season == std::stoi(*data_to_update.max_volume_season)) {
		data_to_update.max_episode_chapter = std::to_string(max_episode);
	}
}
