#ifndef EP_API_SPEC_ANILIST_HPP
#define EP_API_SPEC_ANILIST_HPP

#include "common/media.hpp"
#include "api/request.hpp"

#include <string_view>
#include <nlohmann/json.hpp>
#include <vector>

namespace ep::api::spec::anilist {
	[[nodiscard]]
	ep::api::request get_request(const std::string_view, const ep::media::type &) noexcept;

	// Constructs a request from an already-existing data object.
	// Useful for postprocessing or otherwise just getting new information about it.
	// Can throw a std::runtime_error if the data is missing a type and/or remote id!
	[[nodiscard]]
	ep::api::request get_request(const ep::media::data &);
	// Gets a single chunk of data from a given JSON object.
	// Does NOT handle JSON arrays/lists/etc.
	// Can throw a std::runtime_error if the JSON isn't an object!
	[[nodiscard]]
	ep::media::data get_data(const nlohmann::json &, const ep::media::type &);
	// Gets the top N (5 or less) data objects out of the JSON.
	[[nodiscard]]
	std::vector<ep::media::data> get_top_data(const nlohmann::json &, const ep::media::type &) noexcept;
}

#endif
