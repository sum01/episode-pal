#ifndef EP_API_SPEC_TVMAZE_HPP
#define EP_API_SPEC_TVMAZE_HPP

#include "common/media.hpp"
#include "api/request.hpp"

#include <string_view>
#include <nlohmann/json.hpp>
#include <vector>

namespace ep::api::spec::tvmaze {
	[[nodiscard]]
	ep::api::request get_request(const std::string_view) noexcept;

	// Useful for post-processing, which is needed because of how this API works.
	// Can throw a std::runtime_error if the data doesn't have a ID.
	[[nodiscard]]
	ep::api::request get_request(const ep::media::data &);

	// Updates pre-existing data with more and/or update-to-date information.
	// Useful for post-processing.
	// Can throw a std::runtime_error if the JSON isn't in object form.
	void update_data(const nlohmann::json &, ep::media::data &);

	// Gets a single chunk of data from a given JSON object.
	// Does NOT handle JSON arrays/lists/etc.
	// Can throw a std::runtime_error if the JSON isn't in object form.
	[[nodiscard]]
	ep::media::data get_data(const nlohmann::json &);

	// Gets the top N (5 or less) data objects out of the JSON.
	[[nodiscard]]
	std::vector<ep::media::data> get_top_data(const nlohmann::json &) noexcept;
}

#endif
