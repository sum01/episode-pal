#include "anilist.hpp"

#include "common/formatting.hpp"
#include "common/json.hpp"
#include "common/log.hpp"

#include <string>
// For std::runtime_error
#include <stdexcept>
// For description formatting
#include <regex>

// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>

// FIXME: <format> isn't implemented in any compilers yet
// #include <format>

// Anilist uses GraphQL for its queries.
// Which gives us some unique issues with forming these requests...

static constexpr std::string_view get_anime_query() noexcept {
	return "query($name: String) { Page(page: 1) { media(search: $name, type:ANIME) { title { romaji english } id episodes description } } }";
}

// Gets only the newest episodes for a specific (remote) ID
// Other info is unimportant.
static constexpr std::string_view get_anime_query_byid() noexcept {
	return "query($id: int) { Media(id: $id, type:ANIME) { episodes } }";
}

// Gets only the newest chapters & volumes for a specific (remote) ID
// Other info is unimportant.
static constexpr std::string_view get_manga_query_byid() noexcept {
	return "query($id: int) { Media(id: $id, type:MANGA) { chapters volumes } }";
}

static constexpr std::string_view get_manga_query() noexcept {
	// volumes are like a season of anime
	// chapters are like an episode of anime
	return "query($name: String) { Page(page: 1) { media(search: $name, type:MANGA) { title { romaji english } id chapters volumes description } } }";
}

// Construct the body from a given name string.
static std::string get_body(const std::string_view query, const std::string_view search_name) noexcept {
	return std::string{"{ \"query\": \""} + query.data() + "\", \"variables\": { \"name\":\"" + search_name.data() + "\" }}";
	// FIXME: <format> isn't implemented in any compilers yet.
	// But this should work once it's implemented...
	// return std::format("\{ \"query\": \"{}\", \"variables\": \"{}\"\}\}", query, search_name);
}

// Construct a body request that passes the ID instead of a name.
static std::string get_body_byid(const std::string_view query, const std::string_view id) noexcept {
	return std::string{"{ \"query\": \""} + query.data() + "\", \"variables\": { \"id\":\"" + id.data() + "\" }}";
	// FIXME: <format> isn't implemented in any compilers yet.
	// But this should work once it's implemented...
	// return std::format("\{ \"query\": \"{}\", \"variables\": \"{}\"\}\}", query, search_name);
}

// Since all requests use the same POST style, separate into its own function.
static ep::api::request form_request(const std::string &body) noexcept {
	return {
		// Not putting https:// here because cpp-httplib seems to require not using it.
		.url = "graphql.anilist.co",
		// No endpoint seems to be what they want?
		.endpoint = "/",
		.body = body,
		.type = "application/json",
		.use_post = true,
		// Anilist seems to not allow compression.
		.use_compression = false
	};
}

ep::api::request ep::api::spec::anilist::get_request(const std::string_view search_name, const ep::media::type &type) noexcept {
	std::string body;

	switch (type) {
		case ep::media::type::MANGA:
			body = get_body(get_manga_query(), search_name);
			break;
		case ep::media::type::ANIME:
			// We fallthrough since we're defaulting to anime
			[[fallthrough]];
		default:
			body = get_body(get_anime_query(), search_name);
			// Default to anime if no type was given, or it wasn't known
			break;
	}

	return form_request(body);
}

ep::api::request ep::api::spec::anilist::get_request(const ep::media::data &pre_data) {
	// Make sure there's a type & remote ID we can use.
	if (!pre_data.type || !pre_data.remote_id) {
		throw std::runtime_error(gettext("The request for data from Anilist has bad/missing type or ID."));
	}

	std::string body;

	// Just doing this so the code is more readable.
	const std::string &type = *pre_data.type;
	using namespace ep::media;

	if (type == to_string(type::MANGA)) {
		body = get_body_byid(get_manga_query_byid(), *pre_data.remote_id);
	} else if (type == to_string(type::MANGA)) {
		body = get_body_byid(get_anime_query_byid(), *pre_data.remote_id);
	} else {
		throw std::runtime_error(gettext("Unknown type given to Anilist's get_request call!"));
	}

	return form_request(body);
}

// Sometimes there's no main name, but there is an alt name.
// In these rare cases, we can replace the name with alt_name
static void replace_name_if_empty(ep::media::data &data) noexcept {
	// Remember these values are <optional>
	if (!data.name || data.name->empty()) {
		// Either copies its data, or if alt_name is empty then it does nothing of value.
		data.name = data.alt_name;
	}
}

ep::media::data ep::api::spec::anilist::get_data(const nlohmann::json &media_object, const ep::media::type &type) {
	if (!media_object.is_object()) {
		throw std::runtime_error(gettext("Anilist was asked to get data from malformed/unexpected JSON."));
	}

	ep::media::data params;
	params.name = ep::json::safe_get<std::string>(media_object.at("title").at("english")),
	params.alt_name = ep::json::safe_get<std::string>(media_object.at("title").at("romaji")),
	params.type = ep::media::to_string(type, true),
	params.description = ep::remove_end_space(ep::process_description(ep::json::safe_get<std::string>(media_object.at("description")))),
	params.remote_id = ep::json::safe_get<int>(media_object.at("id")),
	params.internal_type = type;

	switch (type) {
		case ep::media::type::MANGA:
			params.max_volume_season = ep::json::safe_get<int>(media_object.at("volumes"));
			params.max_episode_chapter = ep::json::safe_get<int>(media_object.at("chapters"));
			break;
		case ep::media::type::ANIME:
			[[fallthrough]];
		default:
			// There isn't a "season" field for anime
			// Instead, each season gets its own new DB object (in Anilist), and will show up in this loop
			params.max_episode_chapter = ep::json::safe_get<int>(media_object.at("episodes"));
			break;
	}
	// Sometimes there's no main name, but there is an alt name.
	// In these rare cases, we can replace the name with alt_name
	replace_name_if_empty(params);
	return params;
}

std::vector<ep::media::data> ep::api::spec::anilist::get_top_data(const nlohmann::json &json, const ep::media::type &type) noexcept {
	std::vector<ep::media::data> data_res;

	// Note I left out the specific fields inside of those objects in the inner media array.
	// The entire response is inside a "data" object like { "data": {"Page": { "media": [{},{},{}] }} }
	// The media object inside of "Page" should be an array.
	// Each array object should correspond to our original query, just with data filled in.
	for (const auto &media_object : json.at("data").at("Page").at("media")) {
		// Actually construct our resulting  from the params
		try {
			data_res.emplace_back(ep::api::spec::anilist::get_data(media_object, type));
		} catch (const std::exception &e) {
			ep::log(e.what());
			continue;
		}
	}

	return data_res;
}
