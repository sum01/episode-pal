#ifndef EP_API_HANDLER_HPP
#define EP_API_HANDLER_HPP

#include "common/media.hpp"

#include <string_view>
#include <vector>

// For holding the query's JSON data
#include <nlohmann/json.hpp>

// Manages our various API's
namespace ep::api::handler {
	// Query should be given a name to search for (so something like "attack on titan").
	// The media type defaults to anime if none is given, but you can define it manually.
	// Returns a nlohmann::json object
	// The JSON should be an array of data for most requests.
	// It's possible, however, that the request was bad and the object is malformed
	// If there are errors with the network or certificate, an empty JSON is returned.
	[[nodiscard]]
	nlohmann::json query(const std::string_view, const ep::media::type = ep::media::type::ANIME) noexcept;
	// query_top_n gets the API's JSON response & returns the top 5 (or less) best matches.
	// There can be less than 5 returned if the API didn't return enough data.
	/* std::vector<nlohmann::json> query_top_n(const std::string_view, const ep::media_type) const noexcept; */
	[[nodiscard]]
	std::vector<ep::media::data> get_top_data(const nlohmann::json &, const ep::media::type &) noexcept;

	// Performs post-processing on the chosen selection.
	// Will (sometimes) perform more network calls.
	// Not all API's need this, but some do.
	// Just always call this to be safe!
	// Afterwards, the data is ready-to-go, and can be added to the treeview.
	void post_process(ep::media::data &) noexcept;
}

#endif
