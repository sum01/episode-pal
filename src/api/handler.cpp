#include "handler.hpp"

#include "spec/anilist.hpp"
#include "spec/tvmaze.hpp"
#include "network.hpp"
#include "common/log.hpp"

// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>
#include <glibmm-2.4/glibmm/ustring.h>

nlohmann::json ep::api::handler::query(const std::string_view query_name, const ep::media::type query_type) noexcept {
	using namespace ep::api::spec;

	switch (query_type) {
		case ep::media::type::TV:
			try {
				// TVMaze only does TV shows
				return ep::api::network::get_data(tvmaze::get_request(query_name.data()));
			} catch (...) {
				// Return empty json since the request failed
				return {};
			}
		case ep::media::type::ANIME:
			[[fallthrough]];
		case ep::media::type::MANGA:
			[[fallthrough]];
		default:
			try {
				// Using the Anilist API as the default
				// It can accept both anime & manga
				return ep::api::network::get_data(anilist::get_request(query_name.data(), query_type));
			} catch (...) {
				// Return empty json since the request failed
				return {};
			}
	}
}

std::vector<ep::media::data> ep::api::handler::get_top_data(const nlohmann::json &json, const ep::media::type &type) noexcept {
	try {
		switch (type) {
			case ep::media::type::TV:
				return ep::api::spec::tvmaze::get_top_data(json);
			case ep::media::type::MOVIE:
			// FIXME: uncomment when movies are implemented!
			/* return ep::api::spec::omnidb::get_top_data(json); */
			case ep::media::type::MANGA:
				[[fallthrough]];
			case ep::media::type::ANIME:
				[[fallthrough]];
			default:
				return ep::api::spec::anilist::get_top_data(json, type);
				break;
		}
	} catch (const std::exception &err) {
		ep::log(err.what());
	} catch (...) {
		ep::log(Glib::ustring::compose(gettext("Unknown exception thrown trying to get the top data of a JSON query for %1 with JSON \"%2\""), ep::media::to_string(type), json.dump()).c_str());
	}
	// Since we're noexcept, just return empty data if we failed
	return {};
}

void ep::api::handler::post_process(ep::media::data &pre_data) noexcept {
	using namespace ep::api::spec;

	switch (pre_data.internal_type) {
		case ep::media::type::ANIME:
			// Anilist doesn't need any post-processing
			return;
		case ep::media::type::MANGA:
			// Anilist doesn't need any post-processing
			return;
		case ep::media::type::MOVIE:
			// TODO: will it need post-processing?
			return;
		case ep::media::type::TV:
			// For now just fallthrough since TV is all that needs post-processing
			[[fallthrough]];
		default:
			try {
				// TVMaze needs post-processing because their API doesn't allow retrieval of all the info on first request.
				tvmaze::update_data(ep::api::network::get_data(tvmaze::get_request(pre_data)), pre_data);
			} catch (const std::exception &e) {
				ep::log(e.what());
				return;
			} catch (...) {
				// TODO: maybe rethink this. idk
				ep::log(Glib::ustring::compose(gettext("Unknown exception thrown trying to post-process data for %1 with name \"%2\""),
																			 pre_data.type.value_or("ERROR-BAD-TYPE"),
																			 pre_data.name.value_or("ERROR-BAD-NAME")).c_str());
			}
	}
}
