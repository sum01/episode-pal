#ifndef EP_API_NETWORK_HPP
#define EP_API_NETWORK_HPP

#include "request.hpp"

#include <nlohmann/json.hpp>

namespace ep::api::network {
	// A simple wrapper over a networking library.
	// Can make POST or GET requests.
	// Note that both construction & get_data can throw.
	// Pass the request to use. Can be used for POST or GET requests.
	// Returns a nlohmann::json with the parsed data.
	// Can throw a std::runtime_error if the request or parsing fails.
	[[nodiscard]]
	nlohmann::json get_data(const ep::api::request &);
}

#endif
