#ifndef EP_API_REQUEST_HPP
#define EP_API_REQUEST_HPP

#include <string>

namespace ep::api {
	// A simple request type struct
	// For now, assume the returned data is in JSON.
	// If this changes in the future, a returns_json field should be added.
	struct request {
		std::string url,
								endpoint = "/",
								body,
								type = "application/json";
		// If false, uses GET
		bool use_post = true,
		// If true, uses gzip compression
				 use_compression = true;
	};
}

#endif
