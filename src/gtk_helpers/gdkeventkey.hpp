#ifndef EP_GTK_HELPERS_GDKEVENTKEY_HPP
#define EP_GTK_HELPERS_GDKEVENTKEY_HPP

#include <string>

// For GdkEventKey
#include <gtk-3.0/gtk/gtk.h>

// Global-scope so other things can use it
// Only compares state & keyval
constexpr bool operator==(const GdkEventKey &, const GdkEventKey &) noexcept;

namespace ep {
	// Converts a passed GdkEventKey to a string representation.
	// This will put it in a format like "MOD+keyvalname"
	// If there's no mod, then just "keyvalname"
	std::string to_string(const GdkEventKey &event);
}

#endif
