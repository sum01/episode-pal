#include "gdkeventkey_wrapper.hpp"
// for ep::to_string
#include "gdkeventkey.hpp"

ep::gdkeventkey_wrapper::gdkeventkey_wrapper(const ep::gdkeventkey_wrapper &that) noexcept {
	this->event = that.event;
}

constexpr bool operator==(const GdkEventKey &lhs, const GdkEventKey &rhs) noexcept {
	return lhs.state == rhs.state && lhs.keyval == rhs.keyval;
}

bool ep::gdkeventkey_wrapper::operator==(const ep::gdkeventkey_wrapper &rhs) const noexcept {
	return this->event == rhs.get_event();
}

const GdkEventKey &ep::gdkeventkey_wrapper::get_event() const noexcept {
	return this->event;
}

void ep::gdkeventkey_wrapper::operator=(const ep::gdkeventkey_wrapper &cpy_tar) noexcept {
	// Used for readability
	this->event = cpy_tar.get_event();
}

ep::gdkeventkey_wrapper::gdkeventkey_wrapper(const GdkEventKey &t) noexcept {
	// For some reason, the state isn't always a useable value. Sometimes it's a a useless value.
	// I assume this is because it's just a non-null pointer with no assigned value.
	// So only preserve if it's in range of all known mods, otherwise default to 0
	this->event.state = t.state <= GDK_MODIFIER_MASK ? t.state : 0;
	this->event.keyval = t.keyval;
}

ep::gdkeventkey_wrapper::gdkeventkey_wrapper(const guint state, const guint keyval) noexcept {
	this->event.state = state;
	this->event.keyval = keyval;
}

ep::gdkeventkey_wrapper::gdkeventkey_wrapper(const guint keyval) noexcept {
	// 0 signifies no modifier
	this->event.state = 0;
	this->event.keyval = keyval;
}

std::string ep::gdkeventkey_wrapper::to_string() const noexcept {
	return ep::to_string(this->event);
}

// Thanks to https://www.techiedelight.com/use-struct-key-std-unordered_map-cpp/ for the example.
std::size_t ep::hash_fn::operator() (const ep::gdkeventkey_wrapper &tar) const noexcept {
	const auto &event = tar.get_event();

	return std::hash<guint>()(event.state) ^ std::hash<guint>()(event.keyval);
}
