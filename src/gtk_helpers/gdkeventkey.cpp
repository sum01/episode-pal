#include "gdkeventkey.hpp"

std::string ep::to_string(const GdkEventKey &event) {
	std::string result;
	// Only bother with states we support
	switch (event.state) {
		case GDK_SHIFT_MASK:
			result = "SHIFT+";
			break;
		case GDK_LOCK_MASK:
			result = "LOCK+";
			break;
		case GDK_CONTROL_MASK:
			result = "CTRL+";
			break;
		case GDK_MOD1_MASK:
			result = "ALT+";
			break;
		case GDK_MOD2_MASK:
			result = "MOD2+";
			break;
		case GDK_MOD3_MASK:
			result = "MOD3+";
			break;
		case GDK_MOD4_MASK:
			result = "MOD4+";
			break;
		case GDK_MOD5_MASK:
			result = "MOD5+";
			break;
		case GDK_BUTTON1_MASK:
			result = "MOUSE1+";
			break;
		case GDK_BUTTON2_MASK:
			result = "MOUSE2+";
			break;
		case GDK_BUTTON3_MASK:
			result = "MOUSE3+";
			break;
		case GDK_BUTTON4_MASK:
			result = "MOUSE4+";
			break;
		case GDK_BUTTON5_MASK:
			result = "MOUSE5+";
			break;
		case GDK_SUPER_MASK:
			result = "SUPER+";
			break;
		case GDK_HYPER_MASK:
			result = "HYPER+";
			break;
		case GDK_META_MASK:
			result = "META+";
			break;
		default:
			break;
	}

	result += gdk_keyval_name(event.keyval);

	return result;
}
