#ifndef EP_GTK_HELPERS_GDKEVENTKEY_WRAPPER_HPP
#define EP_GTK_HELPERS_GDKEVENTKEY_WRAPPER_HPP

#include <string>

// For GdkEventKey
#include <gtk-3.0/gtk/gtk.h>

namespace ep {
	// A simplistic wrapper around the GdkEventKey so we can use it in std containers.
	// Since we can't change Gtk's source code :)
	class gdkeventkey_wrapper {
public:
		explicit gdkeventkey_wrapper(const ep::gdkeventkey_wrapper &) noexcept;
		// Construct with a state & keyval into ours (in that order)
		explicit gdkeventkey_wrapper(const guint, const guint) noexcept;
		// Explicitly copy all values of a given GdkEventKey
		explicit gdkeventkey_wrapper(const GdkEventKey &) noexcept;
		// Copyies as a keyval into ours.
		// Sets the state to 0.
		explicit gdkeventkey_wrapper(const guint) noexcept;
		// Only state & keyval are compared.
		bool operator==(const ep::gdkeventkey_wrapper &) const noexcept;
		// Copy-assign
		void operator=(const ep::gdkeventkey_wrapper &) noexcept;
		const GdkEventKey &get_event() const noexcept;
		// Simply uses ep::to_string from gtk_helpers/gdkeventkey.hpp
		std::string to_string() const noexcept;
private:
		GdkEventKey event;
	};

	// Specialized hash function for maps to use gdkeventkey_wrapper as keys.
	struct hash_fn {
		// Hashes the state & keyval into a single value
		std::size_t operator() (const gdkeventkey_wrapper &) const noexcept;
	};
}

#endif
