#include "main_gui.hpp"
#include "glade_loader.hpp"
#include "helper.hpp"
#include "api/handler.hpp"
#include "common/log.hpp"

#include <string>

#include <nlohmann/json.hpp>
// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>
#include <glibmm-2.4/glibmm/ustring.h>
// For GDK_KEY_XXXXX
#include <gtk-3.0/gdk/gdkkeysyms.h>

ep::gui::main_gui::main_gui() : builder(Gtk::Builder::create()) {
	// main.glade is the main window for displaying all content.
	// If this fails, it'll throw an error.
	// We can just let it stop the program since there's no recovery.
	this->builder->add_from_file(ep::gui::glade_loader::get_file("main"));
	// Sets up our keybinding keymap.
	// Used for certain keypresses
	/* this->load_keymaps(); */

	// main_window is the entire frame
	ep::gui::get_widget_pointer<Gtk::Window>(this->builder, "main_window", this->main_window);
	// Needed so we can hook into the button on_click signals
	ep::gui::get_widget_pointer<Gtk::Button>(this->builder, "anime_button", this->anime_button);
	ep::gui::get_widget_pointer<Gtk::Button>(this->builder, "manga_button", this->manga_button);
	ep::gui::get_widget_pointer<Gtk::Button>(this->builder, "tv_button", this->tv_button);
	ep::gui::get_widget_pointer<Gtk::Button>(this->builder, "movie_button", this->movie_button);

	ep::gui::get_widget_pointer<Gtk::Button>(this->builder, "new_row_button", this->new_row_button);
	ep::gui::get_widget_pointer<Gtk::Button>(this->builder, "update_row_button", this->update_row_button);

	// Menuitems
	ep::gui::get_widget_pointer<Gtk::ImageMenuItem>(this->builder, "menubar_delete_row", this->delete_row_item);
	ep::gui::get_widget_pointer<Gtk::CheckMenuItem>(this->builder, "menubar_hide_description", this->hide_description_item);

	ep::gui::get_widget_pointer<Gtk::Popover>(this->builder, "settings_popover", this->settings_popover);
	// Needed so we can get its text, and hook onto the "on typing" signal
	ep::gui::get_widget_pointer<Gtk::SearchEntry>(this->builder, "search_entry", this->search_entry);

	// Needed so we can add widgets (data) to the list
	// Passing the search_entry tells the TreeView to use our searchbar to search for things as you type.
	this->tree_view = std::make_unique<ep::gui::data_view>(this->builder, *this->search_entry);
	// Our popover that goes under the search bar, and its listbox

	// Actually make our search_popover
	// Had to delay construction since it needs the builder after it's loaded the Glade file
	this->search_popover = std::make_unique<ep::gui::search_popover>(this->builder);

	// Hook into the key release event
	this->main_window->signal_key_release_event().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_key_released));

	// Just setting all the on-click signal handlers to their respective methods
	this->anime_button->signal_clicked().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_anime_button_clicked));
	this->manga_button->signal_clicked().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_manga_button_clicked));
	this->tv_button->signal_clicked().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_tv_button_clicked));
	this->movie_button->signal_clicked().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_movie_button_clicked));

	this->update_row_button->signal_clicked().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_update_row_button_clicked));
	this->new_row_button->signal_clicked().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_new_button_clicked));

	this->delete_row_item->signal_activate().connect(sigc::mem_fun(*this, &ep::gui::main_gui::delete_selected));
	this->hide_description_item->signal_activate().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_hide_description_item_clicked));

	// There's a signal_row_clicked, but that's not what we want. It triggers if/when you use the arrow keys (instead of mouse).
	// signal_row_activated is only on the actual click event (or the return key).
	this->search_popover->get_popover_listbox().signal_row_activated().connect(sigc::mem_fun(*this, &ep::gui::main_gui::on_search_popover_listbox_clicked));

	if (this->conf.has_movie_api_key()) {
		// Since it's a lower opacity as a "disabled" indicator, set it back to 1 for regular opacity
		this->movie_button->property_opacity() = 1.0;
		this->movie_button->property_sensitive() = true;
		this->movie_button->property_can_focus() = true;
	}
}

Gtk::Window &ep::gui::main_gui::get_window() const noexcept {
	return *this->main_window;
}

void ep::gui::main_gui::on_new_button_clicked() noexcept {
	this->new_row(ep::media::data {});
}

// TODO: If an error is found, push it to some sort of status bar label thing.
void ep::gui::main_gui::on_click(const ep::media::type &type) noexcept {
	// Grab the text out of the search bar
	const std::string search_str = this->search_entry->get_text();
	// Cancel early if there's no search text
	if (search_str.empty()) {
		return;
	}

	try {
		// The query can potentially throw, so we try-catch
		const nlohmann::json response = ep::api::handler::query(search_str, type);
		// It's possible the response is empty/garbage
		if (response.empty()) {
			/*
				A generic failure message for if our request to the API gave us nothing back.
				This doesn't mean it actually failed though, just returned nothing.
				The %1 will be substituted with the string of the media type (so Anime/Manga/TV/Movie/etc.)
				So for example, it'll be "The anime API returned nothing when we requested data!"
			*/
			ep::log(Glib::ustring::compose(gettext("The %1 API returned nothing when we requested data!"), ep::media::to_string(type)).c_str());
		} else {
			/* // Clear out the old data */
			/* this->search_popover->delete_data(); */
			// Go through each ep::media::data from the response & save them all
			for (const auto &media_data : ep::api::handler::get_top_data(response, type)) {
				// Actually saves & display the good data
				// This'll construct all the necessary Gtk::Entry and Gtk::label for display
				this->search_popover->save_data(media_data);
			}
			// Actually display the results
			this->search_popover->show();
		}
	} catch (...) {
		/*
			The %1 will be substituted with the string of the media type (so Anime/Manga/TV/Movie/etc.)
			So for example, it'll be "Failed when trying to query the anime API!"
		*/
		ep::log(Glib::ustring::compose(gettext("Failed when trying to query the %1 API for data!"), ep::media::to_string(type)).c_str());
	}
}

void ep::gui::main_gui::on_anime_button_clicked() noexcept {
	this->on_click(ep::media::type::ANIME);
}

void ep::gui::main_gui::on_manga_button_clicked() noexcept {
	this->on_click(ep::media::type::MANGA);
}

void ep::gui::main_gui::on_tv_button_clicked() noexcept {
	this->on_click(ep::media::type::TV);
}

void ep::gui::main_gui::on_movie_button_clicked() noexcept {
	/* this->on_click(ep::media::type::MOVIE); */
}

void ep::gui::main_gui::on_update_row_button_clicked() noexcept {
	// TODO
}

void ep::gui::main_gui::new_row(const ep::media::data &dat) noexcept {
	this->tree_view->prepend_data(dat);
}

void ep::gui::main_gui::delete_selected() noexcept {
	this->tree_view->erase_selected();
}

// No var on purpose, it's unused since I don't need it.
// When they click the listbox element in the popover, we want to save it to our actual listbox
void ep::gui::main_gui::on_search_popover_listbox_clicked(const Gtk::ListBoxRow *) noexcept {
	// Gets the data that's been clicked in the search popover
	ep::media::data selected_data = this->search_popover->get_selected_data();
	// Does any post-processing (i.e. secondary queries) that might be needed.
	ep::api::handler::post_process(selected_data);
	// Actually displays the data in the main treeview
	this->new_row(selected_data);
	// Now that we're done with the data we got, close it.
	// This'll auto-delete old data
	this->search_popover->hide();
}

void ep::gui::main_gui::on_hide_description_item_clicked() noexcept {
	// Enable/disable the description when the user has clicked the menu item.
	// This doesn't go into undo/redo history.
	this->tree_view->set_column_visibile(ep::media::data_i::DESC, !this->hide_description_item->property_active());
}

void ep::gui::main_gui::move_row(const ep::gui::move mv) noexcept {
	this->tree_view->move_selected(mv);
}

bool ep::gui::main_gui::on_key_released(GdkEventKey *event) noexcept {
	// Prevents typing in the search bar from triggering the key releases.
	if (this->main_window->get_focus() == this->search_entry.get()) {
		return false;
	}
	// Make sure it's a valid pointer, and it's an actual keypress
	// Using release because I think that's the more preferred functionality.
	if (!event || event->type != GDK_KEY_RELEASE) {
		// Return false tells it to propogate the handler more
		return false;
	}

	// It's an <optional>
	const auto translated_event = this->conf.translate_event(*event);
	// False if we failed to translate it.
	if (!translated_event) {
		return false;
	}

	// Simple lambda to reduce verbosity
	auto is_event = [&translated_event, this](const ep::config::event e) -> bool {
										return (*translated_event) == this->conf.supported_events[static_cast<std::size_t>(e)];
									};

	// Compare the translated event to our known events
	// Perform the coreesponding event if we found a match
	if (is_event(ep::config::event::DELETE)) {
		this->delete_selected();
	} else if (is_event(ep::config::event::UNDO)) {
		this->tree_view->undo_last();
	} else if (is_event(ep::config::event::REDO)) {
		this->tree_view->redo_last();
	} else if (is_event(ep::config::event::UP)) {
		this->move_row(ep::gui::move::UP);
	} else if (is_event(ep::config::event::DOWN)) {
		this->move_row(ep::gui::move::DOWN);
	}

	// Return false tells it to propogate the handler more
	return false;
}
