#include "config.hpp"
#include "common/dirs.hpp"
#include "common/file_reader.hpp"
#include "common/log.hpp"
// for ep::to_string
#include "gtk_helpers/gdkeventkey.hpp"

#include <string_view>
#include <stdexcept>

#include <nlohmann/json.hpp>
// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>
#include <glibmm-2.4/glibmm/ustring.h>

// Sets up all our various keymaps from any potential config files.
// Otherwise just uses default values (aka assumes no rebound keys).
void ep::config::load_config() noexcept {
	// Clear old keybindings & settings
	this->movie_api_key.reset();
	this->keymap.clear();

	// Putting this here so we only call gettext on the string once
	static const std::string_view error_msg = gettext("Failed to parse your config file (invalid JSON). Falling back to the program defaults.");

	const std::optional<std::string> config_string = ep::read_file(ep::get_xdg_config_dir() + "/config.json");
	// Needs to be accessible outside the try-catch parse...
	// unless we want a massive, ugly nested block of code.
	std::optional<const nlohmann::json> config_json;
	// Make sure we actually read something
	if (config_string) {
		try {
			// Will be defined (by Cmake) if the Nlohmann_json version is >= 3.9
			// as that version supports parsing JSON with comments.
			#ifdef EP_ENABLE_JSON_COMMENTS
				// first true is exceptions, second is comments
				config_json.emplace(nlohmann::json::parse(config_string.value(), nullptr, true, true));
			#else
				// Parse the config file into a JSON object for easier reading (since it should be JSON format)
				// This can throw an error if it failed to parse, so we need to try-catch
				config_json.emplace(nlohmann::json::parse(config_string.value()));
			#endif
		} catch (const nlohmann::json::parse_error &e) {
			ep::log(e.what() + std::string {"\n"} + error_msg.data());
		} catch (...) {
			ep::log(error_msg);
		}
	} else {
		ep::log(gettext("Didn't find a config file, using program defaults."));
	}

	if (config_json) {
		// Would be something like "omdb_key": "SOMEKEYHERE"
		const auto movie_key_iter = config_json->find("omdb_key");
		// If there's a movie api key, just save it
		if (movie_key_iter != config_json->end()) {
			this->movie_api_key = *movie_key_iter;
			// A simple logged message if we found their OMDB API key
			ep::log(Glib::ustring::compose(gettext("Loaded OMDB API key \"%1\""), this->movie_api_key.value()).c_str());
		}
	}
	// Setup defaults for any key not set yet.
	// Add more keys as needed
	for (const auto &key : this->supported_events) {
		if (!config_json) {
			// If there was no config at all, just use the event as a default
			this->keymap.emplace(key, key);
			continue;
		}
		// Look to see if they even set a keybinding for the GDK key.
		// Turn the key into a string since the config is a string.
		const auto tar_key_str = config_json->find(ep::to_string(key.get_event()));
		// True if they actually set a custom keybinding.
		if (tar_key_str != config_json->cend()) {
			// Convert the string representation in the JSON config file to an actual GdkEventKey
			const auto custom_key = ep::to_GdkEventKey(tar_key_str.value());
			// Since it might be an invalid string, we need to test it.
			if (custom_key) {
				// Since they actually set a different key, save it to our keymap.
				// Use insert_or_assign so we can re-write previous values if reloading a config file during runtime.
				const ep::gdkeventkey_wrapper custom_keybind {custom_key.value()};
				const auto insert_res = this->keymap.emplace(custom_keybind, key);
				// Check if the insert succeeded or not
				if (insert_res.second) {
					// A little success message when we loaded their custom keybinding. %1 will be their custom keybind, %2 will be the actual corresponding keybind.
					ep::log(Glib::ustring::compose(gettext("Using your custom keybinding \"%1\" for keybinding \"%2\""), custom_keybind.to_string(), key.to_string()).c_str());
				} else {
					// A little failure message when we failed to load their custom keybinding. %1 will be their custom keybind, %2 will be the actual corresponding keybind.
					ep::log(Glib::ustring::compose(gettext("Failed to use your custom keybinding \"%1\" for keybinding \"%2\"!"), custom_keybind.to_string(), key.to_string()).c_str());
				}
			}
		} else {
			// If there was no set keybind for this event, use the default.
			// This will only trigger if they've changed some keybindings, but not all of them.
			this->keymap.emplace(key, key);
		}
	}
}

ep::config::config() noexcept {
	this->load_config();
}

std::optional<ep::gdkeventkey_wrapper> ep::config::translate_event(GdkEventKey unknown_event) noexcept {
	// This seems to be required to compare to a target modmask, as the state isn't the same value without it.
	unknown_event.state = unknown_event.state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK | GDK_MOD1_MASK);

	const auto iter = this->keymap.find(ep::gdkeventkey_wrapper {unknown_event});
	// Only return something if we found a result
	if (iter != this->keymap.end()) {
		// the "second" in the map is the saved event from the supported_events array.
		// "first" would just be the key, which is potentially from a config file.
		return std::make_optional(iter->second);
	} else {
		/* ep::log(Glib::ustring::compose("Failed to translate event with state \"%1\" and keyval \"%2\"", unknown_event.state, gdk_keyval_name(unknown_event.keyval)).c_str()); */
		return std::nullopt;
	}
}

std::optional<GdkEventKey> ep::to_GdkEventKey(const std::string &str) noexcept {
	// The format is "MOD+KEYVAL" or just "KEYVAL"
	// So we search for a plus as the separator.
	const auto plus_pos = str.find("+");
	GdkEventKey result;
	// Tries to save the string as a keyval in the "result" GdkEventKey
	// Returns a success/fail boolean
	auto str_to_keyval = [&result](const std::string_view s) -> std::optional<GdkEventKey> {
												// Putting outside of try-catch so we don't need to have duplicate gettext strings
												 const guint res = gdk_keyval_from_name(s.data());
												// gdk_keyval_from_name returns GDK_KEY_VoidSymbol if it failed
												 if (res == GDK_KEY_VoidSymbol) {
													 ep::log(Glib::ustring::compose(gettext("Invalid keyval used in your custom keybinding with value \"%1\""), s.data()).c_str());
													 return std::nullopt;
												 } else {
													 result.keyval = res;
													 return result;
												 }
											 };
	// If true, we found a "+" which means there's a modifier.
	if (plus_pos != str.npos) {
		// Since modifiers must always be like "CTRL+" we can always start at 0.
		const auto mod_str = str.substr(0, plus_pos);

		// These are our currently supported bindings (as strings)
		if (mod_str == "CTRL") {
			result.state = GDK_CONTROL_MASK;
		} else if (mod_str == "ALT") {
			// MOD1 is "normally" alt as per the docs.
			result.state = GDK_MOD1_MASK;
		} else {
			// Means they used an invalid modifier.
			// Report the error.
			ep::log(Glib::ustring::compose(gettext("Invalid modifier used in your custom keybinding with value \"%1\""), mod_str).c_str());
			// Return nothing since it failed to read the string as an event.
			return std::nullopt;
		}
		// Now we get its keyval from the second part of the string
		return str_to_keyval(str.substr(plus_pos, str.size()));
	} else {
		return str_to_keyval(str);
	}
}

const std::optional<std::string> & ep::config::get_movie_api_key() const noexcept {
	return this->movie_api_key;
}
bool ep::config::has_movie_api_key() const noexcept {
	return this->movie_api_key.has_value();
}
