#ifndef EP_SEARCH_POPOVER_HPP
#define EP_SEARCH_POPOVER_HPP

#include "common/media.hpp"

#include <optional>
// for pair
#include <utility>
// for unique_ptr
#include <memory>

#include <gtkmm-3.0/gtkmm/popover.h>
#include <gtkmm-3.0/gtkmm/listbox.h>
#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <gtkmm-3.0/gtkmm/builder.h>

namespace ep::gui {
	namespace detail {
		struct search_field {
			// A vertical 2 column grid
			Gtk::Grid outer_grid,
			// A horizontal 2 row grid in the left column of outer_grid
								inner_grid;
			Gtk::Label name_label,
								 description_label;
			std::optional<Gtk::Label> alt_name_label;
		};
	}
	// Manages the search results from the API
	// Meant to be used for the search_popover under the search_entry
	// Each element is a grid of labels & corresponding text entries.
	class search_popover {
public:
		// Pass the builder it should use to load, and an object you want it to attach to.
		explicit search_popover(Glib::RefPtr<Gtk::Builder> &) noexcept;

		[[nodiscard]]
		const Gtk::Popover &get_popover() const noexcept;
		// Returns the Gtk::ListBox
		// Can be used to setup signals for on_click of listbox elements
		[[nodiscard]]
		Gtk::ListBox &get_popover_listbox() noexcept;

		// Hides the search popover
		// Calls delete_data for you automatically
		void hide() noexcept;
		// Shows the search popover
		// Unselects any selected rows automatically
		void show() noexcept;

		// Save a chunk of data as fields for the search popover listbox
		// Takes ownership of the passed data
		void save_data(const ep::media::data &) noexcept;
		// Deletes all the previously saved fields
		// The widgets will vanish from the GUI if still being displayed!
		void delete_data() noexcept;

		// Finds the desired ep::media::data according to what's currently selected
		// Will throw with std::logic_error if you try to get data when there is none
		[[nodiscard]]
		const ep::media::data &get_selected_data();

		// Clears any selections in the listbox
		void clear_selection() noexcept;
private:
		// The actual floating window we fill with dynamic data
		std::unique_ptr<Gtk::Popover> popover;
		// The ListBox holds the GUI elements we'll dynamically add.
		std::unique_ptr<Gtk::ListBox> list_box;
		// Saving the specific search_field's we've made
		// also saves the literal data "by its side" in the pair
		std::vector<std::pair<ep::gui::detail::search_field, ep::media::data> > data;
	};
}

#endif
