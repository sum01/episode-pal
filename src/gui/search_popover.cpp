#include "search_popover.hpp"
#include "helper.hpp"

// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>
// Needed for Glib::Markup::escape_text
#include <glibmm-2.4/glibmm/markup.h>
#include <glibmm-2.4/glibmm/ustring.h>

#include <stdexcept>
#include <string_view>

// Tells the popover its position will be relative to the passed widget
// This will be the search bar (SearchEntry)
ep::gui::search_popover::search_popover(Glib::RefPtr<Gtk::Builder> &builder) noexcept :
	popover(get_widget_pointer<Gtk::Popover>(builder, "search_popover")),
	list_box(get_widget_pointer<Gtk::ListBox>(builder, "search_popover_listbox")) {
	// Loads in the search_popover and search_popover_listbox so we can manipulate them.
	/* ep::gui::get_widget_pointer<Gtk::Popover>(builder, "search_popover", this->popover); */
	/* ep::gui::get_widget_pointer<Gtk::ListBox>(builder, "search_popover_listbox", this->list_box); */
}

const Gtk::Popover &ep::gui::search_popover::get_popover() const noexcept {
	return *this->popover;
}

void ep::gui::search_popover::hide() noexcept {
	// Since it's common use-case is to delete the data when hidden
	// just do it here.
	this->delete_data();

	this->popover->hide();
}

void ep::gui::search_popover::show() noexcept {
	this->clear_selection();
	this->popover->show();
}

Gtk::ListBox &ep::gui::search_popover::get_popover_listbox() noexcept {
	return *this->list_box;
}

void ep::gui::search_popover::delete_data() noexcept {
	// Return early if there's no data
	// since it'd be a waste of time to go further
	if (this->data.empty()) {
		return;
	}
	// Remove all the widgets in the listbox
	// This effectively clears all the rows
	this->list_box->foreach([this](Gtk::Widget &widget){
		this->list_box->remove(widget);
	});
	// Now actually erase the widgets & API data since we don't need it
	this->data.clear();
}

void ep::gui::search_popover::clear_selection() noexcept {
	// Unselects the currently selected row (if any)
	// We don't need to "unselect_all" since we set a "selection single" mode in the constructor
	this->list_box->unselect_row();
}

const ep::media::data &ep::gui::search_popover::get_selected_data() {
	if (this->data.empty()) {
		throw std::logic_error(gettext("Tried to get data from the search popover when there was none!"));
	}

	// selected_child should be a Gtk::Grid in this->data (or nullptr)
	// Specifically, an ep::gui::detail::search_field equal to an outer_grid
	const Gtk::Widget *selected_child = this->list_box->get_selected_row()->get_child();

	// This shouldn't be nullptr, but supposedly it can happen.
	if (!selected_child) {
		// If this triggered, it's because there was some odd issue where GTK returned nothing...
		// when we tried to get the selected row.
		throw std::logic_error(gettext("Tried to get the selected row's widget in the search popover, but there was none!"));
	}

	for (auto &pair_data : this->data) {
		// NOTE: might need to compare in a different way?
		// Just check if they're at the same memory position to determine equality
		if (&pair_data.first.outer_grid == selected_child) {
			// the .second here is the data that we initially built this grid with
			return pair_data.second;
		}
	}
	// This should probably never trigger, and is mostly just here so code compiles.
	throw std::logic_error(gettext("Failed to find relevant data from the selected row in the search popover!"));
}

// Returning a Glib::ustring since escape_text creates one.
// and then the bold-wrapped string will be passed to something taking one anyways.
// This avoids creating a Glib::ustring, then a std::string, then a Glib::ustring again.
static Glib::ustring wrap_bold(const std::string_view str) noexcept {
	// Need to escape text since we can't expect the API results to always be safe
	return "<b>" + Glib::Markup::escape_text(str.data()) + "</b>";
}

static void setup_label(Gtk::Label &label) noexcept {
	// Line wrap is mostly useful for the description..
	// but even some names can be really long
	label.set_line_wrap(true);
	// Makes sure the label aligns to the left of the grid..
	// since its default is center
	label.set_halign(Gtk::Align::ALIGN_START);
	// Prevents the line-wrap from seemingly adding padding.
	label.set_xalign(0.0f);
	// (x, y) padding around the label.
	// Just makes sure the text isn't directly against the side of the grid
	label.set_padding(2, 3);
	// Lets us see the label
	label.show();
}

// Puts each part of the given media data into text boxes on the display grid
// Only handles 1 data struct at a time
void ep::gui::search_popover::save_data(const ep::media::data &data) noexcept {
	// Note that not all of these may be used, since movies don't have episodes/seasons/etc.
	this->data.emplace_back(ep::gui::detail::search_field {}, data);
	// Shorthand references to reduce verbosity in the code
	auto &d_b = this->data.back();
	ep::gui::detail::search_field &sf = d_b.first;

	// set_markup lets us apply bolding to the text
	sf.name_label.set_markup(wrap_bold(data.name.value_or("")));
	setup_label(sf.name_label);

	sf.description_label.set_text(data.description.value_or(""));
	setup_label(sf.description_label);

	// Remember that alt_name_label is an <optional>
	if (data.alt_name) {
		// Since it was nullopt before, construct it here
		// Now we can set_text on the object since it exists.
		// Don't need value_or since we checked if it exists above..
		sf.alt_name_label.emplace().set_text(data.alt_name.value());
		setup_label(*sf.alt_name_label);
	}

	// ~~~~~ Example layout ~~~~
	// Name      Description
	// Alt_name

	/* this->data.back().first.outer_grid.set_row_spacing(0); */
	d_b.first.outer_grid.set_orientation(Gtk::Orientation::ORIENTATION_VERTICAL);
	// this->data.back().first.outer_grid.set_row_homogeneous(true);
	// We don't want homogeneous because the description takes up a lot more space
	d_b.first.outer_grid.set_column_homogeneous(false);
	// Puts some space between the names & description so they are easier to read
	d_b.first.outer_grid.set_column_spacing(4);
	// .first is our search_field, .second is the data
	/* this->data.back().first.outer_grid.set_halign(Gtk::Align::ALIGN_START); */
	/* this->data.back().first.outer_grid.set_valign(Gtk::Align::ALIGN_START); */
	// The inner grid is on the left of the outer_grid
	// It will have 2 rows, 1 column
	d_b.first.inner_grid.set_orientation(Gtk::Orientation::ORIENTATION_HORIZONTAL);
	// Make sure content left-aligns
	/* this->data.back().first.inner_grid.set_halign(Gtk::Align::ALIGN_START); */
	/* this->data.back().first.inner_grid.set_valign(Gtk::Align::ALIGN_START); */

	// Remember that inner_grid is 1 column, 2 rows
	// Also note that the first number is column, and second number is row index
	// Put the name widget in top-left corner of the grid
	d_b.first.inner_grid.attach(d_b.first.name_label, 0, 0);
	// Remember that alt_name_label is an <optional>
	if (d_b.first.alt_name_label.has_value()) {
		// If there's an alt_name, put it under the name
		d_b.first.inner_grid.attach(d_b.first.alt_name_label.value(), 0, 1);
	}

	d_b.first.inner_grid.show();
	d_b.first.outer_grid.attach(d_b.first.inner_grid, 0, 0);
	// Puts the description to the side
	d_b.first.outer_grid.attach(d_b.first.description_label, 1, 0);
	d_b.first.outer_grid.show();

	// Actually append the outer_grid into our list_box
	// This should let it be seen in the Gtk::ListBox
	this->list_box->append(d_b.first.outer_grid);
}
