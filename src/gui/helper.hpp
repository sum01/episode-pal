#ifndef EP_GUI_HELPER_HPP
#define EP_GUI_HELPER_HPP

#include <memory>
#include <string_view>
#include <stdexcept>
#include <concepts>

#include <gtkmm-3.0/gtkmm/builder.h>
#include <gtkmm-3.0/gtkmm/widget.h>
#include <glibmm/object.h>
// Needed for gettext
#include <glibmm/i18n.h>

namespace ep::gui {
	namespace detail {
		// Some widgets also derive from Glib::Object
		// So this is kind of a safety-check on that
		template<typename T>
		concept object = std::derived_from<T, Glib::Object> && !std::derived_from<T, Gtk::Widget>;
	}
	// Simply get the target object's Glib::RefPtr
	// Should (and can because of concepts) only be used if it's not a widget!
	template<typename T>
	requires detail::object<T>
	[[nodiscard]]
	Glib::RefPtr<T> get_object_pointer(Glib::RefPtr<Gtk::Builder> &builder, const std::string_view id) {
		// We have to cast since it returns a Gtk::Object instead of T
		// Can't use static_cast because of RefPtr peculiarities.
		Glib::RefPtr<T> res = Glib::RefPtr<T>::cast_static(builder->get_object(id.data()));
		// Check if we actually got a valid object pointer
		if (!res) {
			// Simple error message. The %1 is replaced with the ID of the object and should not be changed!
			throw std::runtime_error(Glib::ustring::compose(gettext("Failed to load the target object \"%1\""), id.data()));
		}
		return res;
	}
	// Gets a widget pointer into the passed unique_ptr.
	// Handles error-checking to make sure the widget was successfully retrieved.
	// Will throw a std::runtime_error if the widget doesn't load.
	template<typename T>
	requires std::derived_from<T, Gtk::Widget>
	void get_widget_pointer(Glib::RefPtr<Gtk::Builder> &builder, const std::string_view id, std::unique_ptr<T> &u_ptr) {
		// Needs to be a dumb pointer so get_widget actually works
		T *temp = nullptr;
		// If this fails, it only prints a warning
		builder->get_widget(id.data(), temp);

		// Sanity check for development in case we try to accidentally use a non-existant widget
		if (!temp) {
			/*
				The %1 is replaced with the ID of the widget.
				So for example, it would be "Failed to load the target widget "search_entry""
			*/
			throw std::runtime_error(Glib::ustring::compose(gettext("Failed to load the target widget \"%1\""), id.data()));
		}

		// Reset takes ownership, so we don't need to delete temp.
		u_ptr.reset(temp);
	}

	// Returns a widget pointer in a unique_ptr.
	// Handles error-checking to make sure the widget was successfully retrieved.
	// Will throw a std::runtime_error if the widget doesn't load.
	template<typename T>
	requires std::derived_from<T, Gtk::Widget>
	std::unique_ptr<T> get_widget_pointer(Glib::RefPtr<Gtk::Builder> &builder, const std::string_view id) {
		// Needs to be a dumb pointer so get_widget actually works
		T *temp = nullptr;
		// If this fails, it only prints a warning
		builder->get_widget(id.data(), temp);

		// Sanity check for development in case we try to accidentally use a non-existant widget
		if (!temp) {
			/*
				The %1 is replaced with the ID of the widget.
				So for example, it would be "Failed to load the target widget "search_entry""
			*/
			throw std::runtime_error(Glib::ustring::compose(gettext("Failed to load the target widget \"%1\""), id.data()));
		}

		std::unique_ptr<T> res {temp};

		return res;
	}
}

#endif
