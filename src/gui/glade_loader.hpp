#ifndef EP_GLADE_LOADER_HPP
#define EP_GLADE_LOADER_HPP

#include <string>

namespace ep::gui::glade_loader {
	// This can throw if the file wasn't found.
	// But first, it looks in multiple locations...
	// such as the local dir, a build dir, and install location.
	// Returns the filepath as a string, since that's what gtkmm uses.
	std::string get_file(const std::string &);
}

#endif
