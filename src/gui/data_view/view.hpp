#ifndef EP_GUI_DATA_VIEW_VIEW_HPP
#define EP_GUI_DATA_VIEW_VIEW_HPP

#include "detail/public_view.hpp"
#include "common/media.hpp"
#include "common/commandable.hpp"

#include <gtkmm-3.0/gtkmm/cellrenderertext.h>

namespace ep::gui {
	// All methods (except set_column_visibile) of this class are commands.
	// They will auto-execute when called, and are added to the command stack.
	class data_view : public ep::commandable,
		private ep::detail::view {
public:
		// The Gtk::Entry you pass should be the textbox used for searching data.
		explicit data_view(Glib::RefPtr<Gtk::Builder> &, Gtk::Entry &);
		/* using ep::detail::view::view; */
		// Pushes data into the top of the TreeView
		void prepend_data(const ep::media::data &) noexcept;
		// Erase the currently-selected row
		void erase_selected() noexcept;
		// Move a selected row up/down
		void move_selected(const ep::gui::move) noexcept;
		// Not a command!
		// Hide/show the target column using the passed enum (index).
		void set_column_visibile(const ep::media::data_i, const bool) noexcept override;
private:
		void setup_edited_signal(Gtk::CellRendererText &, const ep::media::data_i) noexcept override;
		void setup_reordered_signal(Gtk::TreeViewColumn &) noexcept override;
	};
}

#endif
