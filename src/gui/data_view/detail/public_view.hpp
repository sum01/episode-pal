#ifndef EP_GUI_DATA_VIEW_DETAIL_VIEW_HPP
#define EP_GUI_DATA_VIEW_DETAIL_VIEW_HPP

// This class is mostly meant to be privately inherited to make a commandable (and only commandable) Gtk::TreeView

#include "common/media.hpp"
#include "columns.hpp"

#include <memory>

#include <gtkmm-3.0/gtkmm/adjustment.h>
#include <gtkmm-3.0/gtkmm/builder.h>
#include <gtkmm-3.0/gtkmm/treeview.h>
#include <gtkmm-3.0/gtkmm/liststore.h>
#include <gtkmm-3.0/gtkmm/entry.h>
#include <gtkmm-3.0/gtkmm/treeiter.h>
#include <gtkmm-3.0/gtkmm/treepath.h>
#include <glibmm-2.4/glibmm/ustring.h>

namespace ep::gui {
	// Used to signify the direction a row of data is to be moved
	enum class move {UP, DOWN};
}

namespace ep::detail {
	// This class is meant to be inherited by a commandable class, and is merely serving as an implementation detail for commands.
	class view {
public:
		// Pass the builder that loaded the Glade file where the TreeView is declared...
		// the Gtk::Entry to tell the TreeView to use for search...
		// Can throw a std::runtime_error in unlikely cases where a column is null.
		// After calling this, you should call view::initial_column_setup()
		explicit view(Glib::RefPtr<Gtk::Builder> &, Gtk::Entry &);
		// Hide/show the target column using the passed enum (index).
		virtual void set_column_visibile(const ep::media::data_i, const bool) noexcept;
		// Allows you to push data to the TreeView as a new row with different positioning based on the iterator's value.
		// See Gtk::ListStore::insert for specific functionality.
		// Default push behaviour is to push above (its index - 1) the passed iter.
		// Returns the iterator pointing to the new row.
		Gtk::TreeIter insert_data(const ep::media::data &, const Gtk::TreeIter &) noexcept;
		// Allows you to push data to the TreeView as a new row at the top.
		// Returns the iterator pointing to the new row.
		Gtk::TreeIter prepend_data(const ep::media::data &) noexcept;
		// Remove last added row.
		// The returned iter points to the next row (std::end if no more).
		// The data is the data we popped off it.
		Gtk::TreeIter pop_top() noexcept;
		// Makes an empty row to the TreeView
		// Returns the iterator pointing to the new row.
		Gtk::TreeIter make_empty_row() noexcept;
		// Selects the row before the passed iter (if it exists)
		void select_at(const Gtk::TreeIter &) noexcept;
		// Get the currently-selected row for manipulation
		Gtk::TreeIter get_selected() noexcept;
		// Get a data object from a target row.
		ep::media::data get_data_at(const Gtk::TreeIter &) noexcept;
		// Get the top (first) row's iter
		// Remember that this can potentially be an iterator == std::end
		Gtk::TreeIter get_top() noexcept;
		// Deletes the currently-selected row from the data
		// The returned iter points to the next row (std::end if no more).
		Gtk::TreeIter delete_selected() noexcept;
		// The returned iter points to the next row (std::end if no more).
		Gtk::TreeIter erase_at(const Gtk::TreeIter &) noexcept;
		// Simple re-order methods for moving rows
		// Bool is whether it succeeded or not
		bool move_iter(const Gtk::TreeIter &, const ep::gui::move) noexcept;
		int get_num_children() noexcept;
		// TODO: some sort of update_data(const std::vector<>::size_type)
		// to let us update the max volume & max episode/chapter fields
		// Get the integer position of the target row

		Gtk::TreeNodeChildren::size_type get_index(const Gtk::TreeIter &) const noexcept;
		// Get the index from a Gtk::TreePath (as a string).
		Gtk::TreeNodeChildren::size_type get_index(const Glib::ustring &) const noexcept;
		Gtk::TreeIter get_iter(const Gtk::TreeNodeChildren::size_type) const noexcept;
protected:
		// Only really useful for derived classes
		view & get() noexcept;
		const view & get() const noexcept;

		// Sets up the various columns display settings.
		// Should be called once (and only once) after the class construction.
		void initial_column_setup() noexcept;
		// Runs anytime a cell is edited.
		// Does nothing by default, and should be used to implement commands.
		virtual void setup_edited_signal(Gtk::CellRendererText &, const ep::media::data_i) noexcept;
		// Runs anytime a column is reordered.
		// Does nothing by default, and should be used to implement commands.
		virtual void setup_reordered_signal(Gtk::TreeViewColumn &) noexcept;

		Glib::RefPtr<Gtk::Adjustment> adjustment;
		// Holds no data, just used for indexing/accessing specific columns.
		const ep::detail::ls_columns list_store_columns;
		// A data structure for the rows of data.
		// Kind of like a 2D vector where the first index is the row, second is the field.
		Glib::RefPtr<Gtk::ListStore> list_store,
																 type_liststore,
																 status_liststore;
		std::unique_ptr<Gtk::TreeView> tree_view;
	};
}

#endif
