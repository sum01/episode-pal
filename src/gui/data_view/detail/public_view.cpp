#include "public_view.hpp"
#include "gui/helper.hpp"
#include "common/log.hpp"

#include <stdexcept>
// for to_string
#include <string>
#include <vector>

#include <gtkmm-3.0/gtkmm/cellrenderertext.h>
#include <gtkmm-3.0/gtkmm/cellrendererspin.h>
#include <gtkmm-3.0/gtkmm/cellrenderercombo.h>

void ep::detail::view::initial_column_setup() noexcept {
	// Reusing this type in the lambda below
	using vec_t = std::vector<Gtk::TreeViewColumn *>;
	// Grab a vector of all the columns so we can set them up in the GUI.
	// These are declared in the Glade file for brevity.
	vec_t columns = this->tree_view->get_columns();

	// Helper method to reduce code duplication
	auto setup_col = [&columns, this](const ep::media::data_i i){
										 auto *column_ptr = columns.at(static_cast<vec_t::size_type>(i));
										 if (!column_ptr) {
											 throw std::runtime_error("The column pointer was null. This should never happen!");
										 }
										// Tells it which specific column it uses when sorting.
										 column_ptr->set_sort_column(this->list_store_columns.get_col_t(i));
									 };

	using namespace ep::media;
	// Simple setup of the columns so they know how to sort.
	// Only needed since we declared the model inside the code vs in Glade.
	// Don't do remote_id since it's not part of the GUI, just the backend-data.
	setup_col(data_i::NAME);
	setup_col(data_i::ALT_NAME);
	setup_col(data_i::TYPE);
	setup_col(data_i::STAT);
	setup_col(data_i::LAST_V_S);
	setup_col(data_i::LAST_EC);
	setup_col(data_i::MAX_V_S);
	setup_col(data_i::MAX_EC);
	setup_col(data_i::NOTES);
	setup_col(data_i::RATING);
	setup_col(data_i::DESC);


	// Does the setup of all the column's CellRenderers
	for (Gtk::TreeViewColumn *tree_column : this->tree_view->get_columns()) {
		// I suppose it could be null in some weird case.
		// Mostly just being safe...
		if (!tree_column) {
			ep::log(gettext("TreeViewColumn was unexpectedly null (view::insert_data)."));
			continue;
		}

		this->setup_reordered_signal(*tree_column);

		// We need to do this because Gtk rearranges the index of our columns when the user reorders them.
		// So for example, if you drag the "name" column to a different spot, it's index is no longer "0".
		// Note that the sort column ID was previously set by us in the constructor, which is why we can find its corresponding index.
		const ep::media::data_i column_index = this->list_store_columns.get_corresponding_data_i(tree_column->get_sort_column_id());

		// Gets the actual CellRenderer so we can apply settings to it.
		// The first cell should always be the one we added since we're using prepend.
		Gtk::CellRenderer *cell_renderer = tree_column->get_first_cell();
		// Docs says it can be null
		if (!cell_renderer) {
			ep::log(gettext("CellRenderer was unexpectedly null (view::insert_data)."));
			continue;
		}

		// 0-value for align tells it to go into the top-left corner of its container.
		// since the default is 0.5
		cell_renderer->set_alignment(0.0f, 0.0f);

		// All of our CellRenderer's inherit from CellRendererText.
		// So this is fine.
		Gtk::CellRendererText *c_r_text = dynamic_cast<Gtk::CellRendererText *>(cell_renderer);
		c_r_text->property_editable() = true;

		this->setup_edited_signal(*c_r_text, column_index);

		// The rating is a different kind of renderer
		if (column_index == ep::media::data_i::RATING) {
			Gtk::CellRendererSpin *c_r_spin = dynamic_cast<Gtk::CellRendererSpin *>(c_r_text);
			// Tells it how to handle incrementing/decrementing when the spin buttons are pressed.
			// Also controls it's min/max allowed values, displayed decimal places, and whatnot.
			// The docs say it's fine to allow all spins to use the same adjustment object.
			c_r_spin->property_adjustment() = this->adjustment;
			// This is strictly the amount of decimals that show in the little floating box when you're editing it.
			// Doesn't affect the actual decimal places in the displayed value (after editing).
			c_r_spin->property_digits() = static_cast<guint>(1);
		} else {
			// Tries to wrap at a full word, but can fallback to characters if spacing is limited.
			c_r_text->property_wrap_mode() = Pango::WrapMode::WRAP_WORD_CHAR;
			// For the big text fields (descript & notes) we want some different settings.
			if (column_index == ep::media::data_i::DESC || column_index == ep::media::data_i::NOTES) {
				// The amount of pixels to start wrapping at.
				// Kind of sucks to force wrapping if you're on a bigger screen, but there's no good way to auto-wrap (from what I can tell).
				// If you leave it as -1, it'll run off the screen.
				// We do a large number since a small number causes the entire row to grow to an insane height.
				c_r_text->property_wrap_width() = 800;
			} else {
				// Wrap long names sort of quickly.
				// Some are a bit absurd.
				c_r_text->property_wrap_width() = 200;
			}
		}

		// The status column is a combobox (CellRendererCombo)
		// So we need to set some additional settings for it
		if (column_index == ep::media::data_i::STAT || column_index == ep::media::data_i::TYPE) {
			Gtk::CellRendererCombo *c_r_combo = dynamic_cast<Gtk::CellRendererCombo *>(c_r_text);
			// Tells it which different values are in the combo dropdown box thingy
			// This is different from the main ListStore, which stores the currently selected value as a gchararray
			if (column_index == ep::media::data_i::STAT) {
				c_r_combo->property_model() = this->status_liststore;
			} else {
				c_r_combo->property_model() = this->type_liststore;
			}
			// The column to draw the values from.
			// Our combo liststore's only have 1 column, so 0 is the only valid value.
			c_r_combo->property_text_column() = 0;
		}
	}
}

ep::detail::view::view(Glib::RefPtr<Gtk::Builder> &builder, Gtk::Entry &search_entry) :
	// initial value, minimum value, maximum value, amount increment on plus/minus press
	adjustment(ep::gui::get_object_pointer<Gtk::Adjustment>(builder, "main_treeview_adjustment")),
	list_store(Gtk::ListStore::create(this->list_store_columns)),
	type_liststore(ep::gui::get_object_pointer<Gtk::ListStore>(builder, "combo_type_liststore")),
	status_liststore(ep::gui::get_object_pointer<Gtk::ListStore>(builder, "combo_status_liststore")),
	// Will auto-add the list_store as its model from the glade file
	tree_view(ep::gui::get_widget_pointer<Gtk::TreeView>(builder, "main_treeview")) {
	// Need to manually set the model since we create it in our code.
	this->tree_view->set_model(this->list_store);
	// Tells the TreeView to use the passed search bar for quickly searching of data
	this->tree_view->set_search_entry(search_entry);
}

Gtk::TreeIter ep::detail::view::insert_data(const ep::media::data &dat, const Gtk::TreeIter &tar_iter) noexcept {
	// Returns an iter to the new (empty) row.
	// Used to access that rows values for settings & whatnot.
	auto ls_iter = this->list_store->insert(tar_iter);

// set_value helper to reduce code duplication
	auto set_value = [&ls_iter, this](const ep::media::data_i &d, const std::optional<std::string> &opt, const std::string &or_val = "") {
										// Need to make the value a Glib::ustring since it complains otherwise.
										 ls_iter->set_value(this->list_store_columns.get_col_t(d), Glib::ustring {opt.value_or(or_val)});
									 };

	// Puts all the data into the empty row we just made.
	// The numbers are the index of the column, which comes from the Glade file.
	// Check the main_liststore in main.glade file for reference to which these should be.
	set_value(ep::media::data_i::NAME, dat.name);
	set_value(ep::media::data_i::ALT_NAME, dat.alt_name);
	// Default to "Anime" if no given string
	set_value(ep::media::data_i::TYPE, dat.type, ep::media::to_string(ep::media::type::ANIME, true));
	// true to capitalize the first letter
	// Using this instead of just a string "Unfinished" because it handles translation/localization
	set_value(ep::media::data_i::STAT, dat.status, ep::media::to_string(ep::media::status::UNFINISHED, true));
	set_value(ep::media::data_i::LAST_V_S, dat.last_volume_season);
	set_value(ep::media::data_i::LAST_EC, dat.last_episode_chapter);
	set_value(ep::media::data_i::MAX_V_S, dat.max_volume_season);
	set_value(ep::media::data_i::MAX_EC, dat.max_episode_chapter);
	set_value(ep::media::data_i::NOTES, dat.notes);
	// Needs to pass a string representation of a float.
	// Otherwise we'll have no initial value, not even a 0.0
	set_value(ep::media::data_i::RATING, dat.rating, "3.0");
	set_value(ep::media::data_i::DESC, dat.description);
	// Doing this lets us copy the value from the ep::media::data into our list_store for storage.
	// Doesn't affect the GUI display.
	set_value(ep::media::data_i::REMOTE, dat.remote_id);

	return ls_iter;
}

Gtk::TreeIter ep::detail::view::prepend_data(const ep::media::data &dat) noexcept {
	// Makes an empty row at the top
	return this->insert_data(dat, this->list_store->children().begin());
}

Gtk::TreeIter ep::detail::view::make_empty_row() noexcept {
	// Pushing an empty data means it'll just use the value_or on the optional.
	// Aka it'll fill everything with default values.
	return this->prepend_data(ep::media::data {});
}

Gtk::TreeIter ep::detail::view::get_selected() noexcept {
	Glib::RefPtr<Gtk::TreeSelection> tree_selection = this->tree_view->get_selection();
	// Stop early if there's no selection
	if (!tree_selection) {
		ep::log(gettext("TreeSelection was unexpectedly null. Returning the end iterator instead (view::get_selected)."));
		return this->list_store->children().end();
	}
	Gtk::TreeModel::iterator iter = tree_selection->get_selected();
	// Stop early if there's some issue with the iterator
	if (!iter) {
		return this->list_store->children().end();
	}

	return iter;
}

Gtk::TreeIter ep::detail::view::delete_selected() noexcept {
	// Deletes that specific row from the list_store, which removes it from the GUI.
	return this->erase_at(this->get_selected());
}

void ep::detail::view::set_column_visibile(const ep::media::data_i target_col_i, const bool make_visible) noexcept {
	// For whatever reason, this->view->get_column(this->list_store_columns.description.index()) doesn't actually get the description 100% of the time.
	// Reordering seems to screw things up, so we need to loop through all the columns, checking if it's the right one.
	for (Gtk::TreeViewColumn *tree_column : this->tree_view->get_columns()) {
		// I suppose it could be null in some weird case.
		// Mostly just being safe...
		if (!tree_column) {
			ep::log(gettext("The TreeViewColumn was unexpectedly null (view::set_column_visibile)."));
			continue;
		}

		// We need to do this because Gtk rearranges the index of our columns when the user reorders them.
		// So for example, if you drag the "name" column to a different spot, it's index is no longer "0".
		// Note that the sort column ID was previously set by us in the constructor, which is why we can find its corresponding index.
		// Check if the current column is the actual desired column they want to hide.
		if (this->list_store_columns.get_corresponding_data_i(tree_column->get_sort_column_id()) == target_col_i) {
			tree_column->set_visible(make_visible);
			// When we change a columns visibility, we (potentially) need to resize the entire row.
			// This is because if, say, the description is hidden, the row height will otherwise stay the same.
			// Aka it will have a height way too big compared to what it should be.
			this->tree_view->columns_autosize();
			return;
		}
	}
}

ep::media::data ep::detail::view::get_data_at(const Gtk::TreeIter &row_iter) noexcept {
	using namespace ep::media;
	ep::media::data results;

	if (!row_iter) {
		return results;
	}

	// Grab all the values and place them into a new ep::media::data object
	results.name = row_iter->get_value(this->list_store_columns.get_col_t(data_i::NAME));
	results.alt_name = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::ALT_NAME));
	results.type = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::TYPE));
	results.status = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::STAT));
	results.last_volume_season = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::LAST_V_S));
	results.last_episode_chapter = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::LAST_EC));
	results.max_volume_season = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::MAX_V_S));
	results.max_episode_chapter = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::MAX_EC));
	results.notes = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::NOTES));
	results.rating = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::RATING));
	results.description = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::DESC));
	results.remote_id = row_iter->get_value(this->list_store_columns.get_col_t(ep::media::data_i::REMOTE));

	// Now delete the data and return the iter + popped off data.
	return results;
}

Gtk::TreeIter ep::detail::view::pop_top() noexcept {
	return this->erase_at(this->list_store->children().begin());
}

void ep::detail::view::select_at(const Gtk::TreeIter &iter) noexcept {
	if (!iter) {
		return;
	}

	// Tell the treeview to select before the target iter
	this->tree_view->get_selection()->select(iter);
}

Gtk::TreeIter ep::detail::view::get_top() noexcept {
	return this->list_store->children().begin();
}

Gtk::TreeIter ep::detail::view::erase_at(const Gtk::TreeIter &row_iter) noexcept {
	if (row_iter) {
		return this->list_store->erase(row_iter);
	} else {
		return this->list_store->children().end();
	}
}

Gtk::TreeNodeChildren::size_type ep::detail::view::get_index(const Gtk::TreeIter &iter) const noexcept {
	Gtk::TreeNodeChildren::size_type i = 0;
	auto children = this->list_store->children();

	for (Gtk::TreeIter child = children.begin(); iter != children.end(); ++child) {
		if (child == iter) {
			return i;
		}
		++i;
	}

	return i;
}

Gtk::TreeIter ep::detail::view::get_iter(const Gtk::TreeNodeChildren::size_type i) const noexcept {
	Gtk::TreeNodeChildren::size_type current_i = 0;
	auto children = this->list_store->children();

	for (Gtk::TreeIter child = children.begin(); child != children.end(); ++child) {
		if (current_i == i) {
			return child;
		}
		++current_i;
	}

	// Default-return the end if we failed to find the iter
	return children.end();
}

bool ep::detail::view::move_iter(const Gtk::TreeIter &current_iter, const ep::gui::move move_enum) noexcept {
	// Make a copy so we can minus/plus
	// This is necessary since Gtkmm doesn't have a operator-(int) or operator+(int)
	auto target_iter = current_iter;

	if (move_enum == ep::gui::move::UP) {
		--target_iter;
	} else {
		++target_iter;
	}

	// Check both are valid before trying to move.
	if (current_iter && target_iter) {
		// Switch their positions with eachother.
		this->list_store->iter_swap(current_iter, target_iter);
		return true;
	} else {
		return false;
	}
}

int ep::detail::view::get_num_children() noexcept {
	return this->list_store->children().size();
}

Gtk::TreeNodeChildren::size_type ep::detail::view::get_index(const Glib::ustring &cell_path) const noexcept {
	return this->get_index(this->list_store->get_iter(cell_path));
}

void ep::detail::view::setup_edited_signal(Gtk::CellRendererText &, const ep::media::data_i) noexcept {
	// Empty on purpose. Meant to be overriden by the derived class.
}

ep::detail::view & ep::detail::view::get() noexcept {
	return *this;
}

const ep::detail::view & ep::detail::view::get() const noexcept {
	return *this;
}

void ep::detail::view::setup_reordered_signal(Gtk::TreeViewColumn &) noexcept {
	// Empty on purpose. Meant to be overriden by the derived class.
}
