#include "columns.hpp"

#include <string>
#include <stdexcept>

ep::detail::ls_columns::ls_columns() noexcept {
	// Creates all the columns in the actual data store.
	// remote_id is only kept in this because we want to save the value. It won't be displayed.
	this->add(this->name);
	this->add(this->alt_name);
	this->add(this->type);
	this->add(this->status);
	this->add(this->last_volume_season);
	this->add(this->last_episode_chapter);
	this->add(this->max_volume_season);
	this->add(this->max_episode_chapter);
	this->add(this->notes);
	this->add(this->rating);
	this->add(this->description);
	this->add(this->remote_id);
}

const ep::detail::col_t & ep::detail::ls_columns::get_col_t(const ep::media::data_i i) const {
	using namespace ep::media;
	// Simple conversion of the enums to the specific column in the model.
	switch (i) {
		case data_i::NAME:
			return this->name;
		case data_i::ALT_NAME:
			return this->alt_name;
		case data_i::TYPE:
			return this->type;
		case data_i::STAT:
			return this->status;
		case data_i::LAST_V_S:
			return this->last_volume_season;
		case data_i::LAST_EC:
			return this->last_episode_chapter;
		case data_i::MAX_V_S:
			return this->max_volume_season;
		case data_i::MAX_EC:
			return this->max_episode_chapter;
		case data_i::NOTES:
			return this->notes;
		case data_i::RATING:
			return this->rating;
		case data_i::DESC:
			return this->description;
		case data_i::REMOTE:
			return this->remote_id;
		default:
			// This shouldn't ever happen.
			throw std::logic_error("Failed to convert the data_i with value \"" + std::to_string(static_cast<int>(i)) + "\" to a ep::detail::col_t.\nThis should never happen. Fix your code!");
	}
}

ep::media::data_i ep::detail::ls_columns::get_corresponding_data_i(const int i) const {
	using namespace ep::media;
	// Simple conversion of the column index to a usable enum
	// Can't use a switch-case because Gtk hates us :(
	if (i == this->name.index()) {
		return data_i::NAME;
	} else if (i == this->alt_name.index()) {
		return data_i::ALT_NAME;
	} else if (i == this->type.index()) {
		return data_i::TYPE;
	} else if (i == this->status.index()) {
		return data_i::STAT;
	} else if (i == this->last_volume_season.index()) {
		return data_i::LAST_V_S;
	} else if (i == this->last_episode_chapter.index()) {
		return data_i::LAST_EC;
	} else if (i == this->max_volume_season.index()) {
		return data_i::MAX_V_S;
	} else if (i == this->max_episode_chapter.index()) {
		return data_i::MAX_EC;
	} else if (i == this->notes.index()) {
		return data_i::NOTES;
	} else if (i == this->rating.index()) {
		return data_i::RATING;
	} else if (i == this->description.index()) {
		return data_i::DESC;
	} else if (i == this->remote_id.index()) {
		return data_i::REMOTE;
	} else {
		// This shouldn't ever happen.
		throw std::logic_error("Failed to convert the column index \"" + std::to_string(i) + "\" to a ep::media::data_i.\nThis should never happen. Fix your code!");
	}
}
