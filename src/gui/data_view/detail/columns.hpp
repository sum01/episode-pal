#ifndef EP_GUI_DATA_VIEW_DETAIL_COLUMNS_HPP
#define EP_GUI_DATA_VIEW_DETAIL_COLUMNS_HPP

#include "common/media.hpp"

#include <gtkmm-3.0/gtkmm/treemodel.h>
#include <gtkmm-3.0/gtkmm/treemodelcolumn.h>
#include <glibmm-2.4/glibmm/ustring.h>

namespace ep::detail {
	// Putting this here since it's accessed elsewhere as a passable type.
	using col_t = Gtk::TreeModelColumn<Glib::ustring>;
	// The model used by the liststore for the structure of the columns.
	// Needed so we can address columns by the record instead of index
	class ls_columns : public Gtk::TreeModel::ColumnRecord,
		public ep::media::main_data_t<col_t> {
public:
		explicit ls_columns() noexcept;
		// Convert a target index to a column type.
		// Will throw a std::logic_error if passed an unknown index.
		const col_t & get_col_t(const ep::media::data_i) const;
		// Pass an index from some sort column since this is unaffected by reordering.
		// Converts that index to the actual index of the target column.
		// Will throw a std::logic_error if passed an unknown index.
		ep::media::data_i get_corresponding_data_i(const int i) const;
	};
}

#endif
