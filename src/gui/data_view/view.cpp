#include "view.hpp"

#include "commands/push.hpp"
#include "commands/delete.hpp"
#include "commands/edited.hpp"
#include "commands/move.hpp"

// Just reuse the view's constructor
ep::gui::data_view::data_view(Glib::RefPtr<Gtk::Builder> &b, Gtk::Entry &e) :
	view(b, e) {
	// Has to be in this constructor instead of the ep::detail::view constructor so it calls our overridden method.
	this->initial_column_setup();
}

void ep::gui::data_view::prepend_data(const ep::media::data &dat) noexcept {
	this->commands.execute(std::make_unique<ep::commands::data_push>(this->view::get(), dat));
}

void ep::gui::data_view::erase_selected() noexcept {
	this->commands.execute(std::make_unique<ep::commands::data_delete>(this->view::get(), this->get_selected()));
}

void ep::gui::data_view::move_selected(const ep::gui::move move_dir) noexcept {
	this->commands.execute(std::make_unique<ep::commands::data_move>(this->view::get(), move_dir));
}

void ep::gui::data_view::setup_edited_signal(Gtk::CellRendererText &c_r_text, const ep::media::data_i column_index) noexcept {
	// Hook into the edited signal so we can catch it and create a command that lets us undo/redo.
	// Very important to allow for undo/redo of the edited text people put in.
	c_r_text.signal_edited().connect([column_index, this](const Glib::ustring &cell_path, const Glib::ustring &new_txt) {
		// Simply save the command with the signal parameters.
		// This will also set the text for us, since connecting to the signal makes it not set the text.
		this->commands.execute(std::make_unique<ep::commands::data_edited>(this->view::get(), this->get_index(cell_path), this->list_store_columns.get_col_t(column_index), new_txt));
	});
}

void ep::gui::data_view::set_column_visibile(const ep::media::data_i i, const bool v) noexcept {
	// Just reuse the already-written method
	this->view::set_column_visibile(i, v);
}

void ep::gui::data_view::setup_reordered_signal(Gtk::TreeViewColumn &tvc) noexcept {
	// There isn't an elegant way I can think of to handle undo/redo with a column sort.
	// So to prevent errors, we must simply clear the command history when a column is sorted..
	tvc.signal_clicked().connect([this]() {
		this->commands.clear_command_history();
	});
}
