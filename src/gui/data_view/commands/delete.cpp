#include "delete.hpp"

ep::commands::data_delete::data_delete(ep::detail::view &t, const Gtk::TreeIter &tr) noexcept :
	tree(t), position(this->tree.get_index(tr)), data(this->tree.get_data_at(tr)) {
}
ep::commands::data_delete::data_delete(ep::detail::view &t, const Gtk::TreeIter &tr, const ep::media::data &dat) noexcept :
	tree(t), position(this->tree.get_index(tr)), data(dat) {
}

ep::commands::data_delete::data_delete(ep::detail::view &t, const Gtk::TreeNodeChildren::size_type i) noexcept :
	tree(t), position(i), data(this->tree.get_data_at(this->tree.get_iter(i))) {
}

void ep::commands::data_delete::delete_row() noexcept {
	// Check if the currently-selected row is the same as the one we're "deleting"
	const auto iter = this->tree.get_iter(this->position);
	if (!iter) {
		return;
	}
	this->was_selected = this->tree.get_selected() == iter;
	// Erase at that position.
	this->tree.erase_at(iter);
}

void ep::commands::data_delete::push_row() noexcept {
	const auto iter = this->tree.get_iter(this->position);
	// Push the data back into the row
	this->tree.insert_data(this->data, iter);
	// Check if it's supposed to be selected
	if (this->was_selected && iter) {
		// Now select it since it was previously.
		this->tree.select_at(iter);
	}
}

void ep::commands::data_delete::execute() noexcept {
	this->delete_row();
}

void ep::commands::data_delete::undo() noexcept {
	this->push_row();
}
