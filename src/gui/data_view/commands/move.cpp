#include "move.hpp"

void ep::commands::data_move::mover(const ep::gui::move mv) noexcept {
	const auto iter = this->tree.get_iter(this->position);
	if (!iter) {
		return;
	}
	// note: this might need to go after moving?
	/* this->tree->select_at(iter); */

	if (this->tree.move_iter(iter, mv)) {
		if (mv == ep::gui::move::UP) {
			--this->position;
		} else {
			++this->position;
		}
	}
}

ep::commands::data_move::data_move(ep::detail::view &t, const ep::gui::move mv) noexcept :
	tree(t), position(t.get_index(t.get_selected())), move_direction(mv){
}

void ep::commands::data_move::execute() noexcept {
	this->mover(this->move_direction);
}

void ep::commands::data_move::undo() noexcept {
	using namespace ep::gui;
	// Flip the direction so we can reuse code
	this->mover((this->move_direction == move::UP) ? move::DOWN : move::UP);
}
