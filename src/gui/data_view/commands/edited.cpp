#include "edited.hpp"

ep::commands::data_edited::data_edited(ep::detail::view &t, const Gtk::TreeNodeChildren::size_type &row_i, const ep::detail::col_t &col, const Glib::ustring &text) noexcept :
	tree(t), row_pos(row_i), cell_col(col), new_text(text) {
}

void ep::commands::data_edited::execute() noexcept  {
	auto iter = this->tree.get_iter(this->row_pos);
	if (iter) {
		// Only bother saving the old text once.
		// Might avoid a pointless copy if multiple redo/undos happen.
		if (!this->got_old_text) {
			// Save the old text before mangling it.
			this->old_text = iter->get_value(this->cell_col);
			this->got_old_text = true;
		}
		iter->set_value(this->cell_col, this->new_text);
	}
}

void ep::commands::data_edited::undo() noexcept {
	auto iter = this->tree.get_iter(this->row_pos);
	if (iter) {
		// Just put the old text back in place
		iter->set_value(this->cell_col, this->old_text);
	}
}
