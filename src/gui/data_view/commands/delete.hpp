#ifndef EP_GUI_DATA_VIEW_COMMANDS_DELETE_HPP
#define EP_GUI_DATA_VIEW_COMMANDS_DELETE_HPP

#include "common/media.hpp"
#include "common/command.hpp"
#include "gui/data_view/detail/public_view.hpp"

// TreeNodeChildren is also from treeiter
#include <gtkmm-3.0/gtkmm/treeiter.h>

namespace ep::commands {
	// Deletes the target element (from the iter) off the treeview.
	class data_delete : public ep::command {
public:
		// The passed iter is the one to delete
		explicit data_delete(ep::detail::view &, const Gtk::TreeIter &) noexcept;
		// The passed Gtk::TreeNodeChildren::size_type is the index of the row to delete
		explicit data_delete(ep::detail::view &, const Gtk::TreeNodeChildren::size_type) noexcept;
		// The passed iter is the one to delete
		explicit data_delete(ep::detail::view &, const Gtk::TreeIter &, const ep::media::data &) noexcept;
		// Calls delete_row by default
		virtual void execute() noexcept;
		// Calls push_row by default
		virtual void undo() noexcept;
protected:
		virtual void push_row() noexcept;
		virtual void delete_row() noexcept;
		ep::detail::view &tree;
		// The row this command executes on.
		const Gtk::TreeNodeChildren::size_type position;
		// Data never changes for a command, so we can keep it const
		const ep::media::data data;
		bool was_selected = false;
	};
}

#endif
