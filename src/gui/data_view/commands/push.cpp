#include "push.hpp"

ep::commands::data_push::data_push(ep::detail::view &t, const ep::media::data &dat) noexcept :
	data_delete(t, t.get_top(), dat) {
}

void ep::commands::data_push::execute() noexcept {
	this->push_row();
}

void ep::commands::data_push::undo() noexcept {
	this->delete_row();
}
