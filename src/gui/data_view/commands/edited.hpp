#ifndef EP_GUI_DATA_VIEW_COMMANDS_EDITED_HPP
#define EP_GUI_DATA_VIEW_COMMANDS_EDITED_HPP

#include "common/media.hpp"
#include "common/command.hpp"
#include "gui/data_view/detail/public_view.hpp"

// TreeNodeChildren is also from treeiter
#include <gtkmm-3.0/gtkmm/treeiter.h>

namespace ep::commands {
	// Handles the edited event on any specific field
	// This will do nothing on initial execute, only redo/undo should do anything.
	// This is because the user is the one putting in the initial text, not us.
	class data_edited : public ep::command {
public:
		// The tree, the row position (index), the column to manipulate, and the new text string
		explicit data_edited(ep::detail::view &, const Gtk::TreeNodeChildren::size_type &, const detail::col_t &, const Glib::ustring &) noexcept;
		// Inserts the new text only if it isn't the first execution.
		void execute() noexcept;
		// Undoes the insertion of the text.
		void undo() noexcept;
private:
		ep::detail::view &tree;
		bool got_old_text = false;
		const Gtk::TreeNodeChildren::size_type row_pos;
		const ep::detail::col_t cell_col;
		// These need to be ustring or set_value doesn't work
		const Glib::ustring new_text;
		Glib::ustring old_text;
	};
}

#endif
