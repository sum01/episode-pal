#ifndef EP_GUI_DATA_VIEW_COMMANDS_MOVE_HPP
#define EP_GUI_DATA_VIEW_COMMANDS_MOVE_HPP

#include "common/command.hpp"
#include "gui/data_view/detail/public_view.hpp"

// Needed for TreeNodeChildren
#include <gtkmm-3.0/gtkmm/treeiter.h>

namespace ep::commands {
	class data_move : public ep::command {
public:
		explicit data_move(ep::detail::view &t, const ep::gui::move mv) noexcept;
		void execute() noexcept;
		void undo() noexcept;
private:
		ep::detail::view &tree;
		// The row this command executes on.
		Gtk::TreeNodeChildren::size_type position;
		const ep::gui::move move_direction;

		void mover(const ep::gui::move mv) noexcept;
	};
}

#endif
