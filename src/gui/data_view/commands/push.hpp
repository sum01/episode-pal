#ifndef EP_GUI_DATA_VIEW_COMMANDS_PUSH_HPP
#define EP_GUI_DATA_VIEW_COMMANDS_PUSH_HPP

// Just using delete in reverse lets us create a push command
#include "delete.hpp"
#include "common/media.hpp"
#include "gui/data_view/detail/public_view.hpp"

namespace ep::commands {
	// Pushes data to the treeview
	// We can re-use delete and simply flip its order of logic
	class data_push : public ep::commands::data_delete {
public:
		explicit data_push(ep::detail::view &, const ep::media::data &) noexcept;
		void execute() noexcept;
		void undo() noexcept;
	};
}

#endif
