#ifndef EP_CONFIG_HPP
#define EP_CONFIG_HPP

#include "gtk_helpers/gdkeventkey_wrapper.hpp"

#include <unordered_map>
#include <optional>
#include <string>
#include <array>
// for size_t
#include <cstddef>

// For GdkEventKey
#include <gtk-3.0/gtk/gtk.h>
// For those specific GDK_KEY_Up and GDK_CONTROL_MASK values (for example).
#include <gtk-3.0/gdk/gdkkeysyms.h>

namespace ep {
	// Controls keybindings to allow users to rebind keys using a config file.
	class config {
public:
		// Sets up the keybindings we use
		explicit config() noexcept;
		// Loads/reloads the config file.
		void load_config() noexcept;
		bool has_movie_api_key() const noexcept;
		const std::optional<std::string> & get_movie_api_key() const noexcept;

		// Return the actual event from a given event.
		// The passed event will be translated based on the users keybindings (aka if they've been changed).
		// The translated event will be the actual event (i.e. a config::delete_event).
		// If there was no known translation, the optional is nullopt.
		std::optional<ep::gdkeventkey_wrapper> translate_event(GdkEventKey) noexcept;
		// Used to access specific events in the supported_events array
		enum class event : std::size_t {DELETE = 0, UNDO, REDO, UP, DOWN};
		// Every key we currently support an action on.
		// Kept separate so we can check if a keypress matches one of these.
		const std::array<ep::gdkeventkey_wrapper, 5> supported_events {
			ep::gdkeventkey_wrapper{GDK_KEY_Delete},
			ep::gdkeventkey_wrapper{GDK_CONTROL_MASK, GDK_KEY_z},
			ep::gdkeventkey_wrapper{GDK_CONTROL_MASK, GDK_KEY_r},
			// GDK_MOD1_MASK is "normally alt" as per the docs
			ep::gdkeventkey_wrapper{GDK_MOD1_MASK, GDK_KEY_Up},
			ep::gdkeventkey_wrapper{GDK_MOD1_MASK, GDK_KEY_Down}
		};
private:
		std::unordered_map<ep::gdkeventkey_wrapper, ep::gdkeventkey_wrapper, ep::hash_fn> keymap;
		// If they have a key in their config, save it here
		std::optional<std::string> movie_api_key;
	};

	// Converts a string like "CTRL+z" to a state & keyval.
	// Capitalization does matter (for non-modifier keys), such that "z" and "Z" are different.
	std::optional<GdkEventKey> to_GdkEventKey(const std::string &) noexcept;
	// Turn a given GdkEventKey into a string in the format we except.
	std::string to_string(const GdkEventKey &);
}

#endif
