#include "glade_loader.hpp"
// Needed for ep::get_xdg_data_dir() and whatnot
#include "common/dirs.hpp"
#include "common/log.hpp"

#include <filesystem>
#include <stdexcept>
#include <vector>
#include <string>
#include <string_view>

#include <gtkmm-3.0/gtkmm/builder.h>
// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>
#include <glibmm-2.4/glibmm/ustring.h>

static void print_used_path(const std::string &p) noexcept {
	// The %1 will be filled with a path to a file we loaded
	ep::log(Glib::ustring::compose(gettext("Successfully loaded Glade file %1"), p).c_str());
}

std::string ep::gui::glade_loader::get_file(const std::string &name) {
	// Simple lambda to make a const string
	auto get_name = [&name]() -> std::string {
										std::string res = name;
										res += ".glade";
										return res;
									};
	// Doing it this way so it's const since we never need to change it
	const auto runtime_dir = std::filesystem::current_path();

	const std::string glade_file = get_name(),
										xdg_data_dir = ep::get_xdg_data_dir(),
										installed_data_dir = ep::get_installed_data_dir();
	// TODO: make constexpr when GCC supports constexpr std::string
	const std::string sep {std::filesystem::path::preferred_separator};
	// TODO: make constexpr when GCC supports constexpr std::string & std::vector
	const std::vector<std::string> root_paths {xdg_data_dir,
																						 installed_data_dir,
																						// Only really leaving these here so it can search locally while building.
																						 "src",
																						 "src" + sep + "glade",
																						 "build" + sep + "glade"};


	// This list will grow as we add more glade files
	for (const std::string_view root : root_paths) {
		// Build the dir so it's "root + /episode_pal/name.glade"
		std::string path {root};
		path += sep + glade_file;

		if (std::filesystem::exists(path)) {
			print_used_path(path);
			return path;
		}
		// Check in the current running dir as well.
		// Mostly for Windows users, people installing all to one dir, or during debugging where we aren't installing.
		path = (runtime_dir / path).string();
		if (std::filesystem::exists(path)) {
			print_used_path(path);
			return path;
		}
	}

	// The %1 will be filled with the path we unsuccessfully attempted to load.
	throw std::runtime_error(Glib::ustring::compose(gettext("Failed to find the desired Glade file %1"), glade_file));
}
