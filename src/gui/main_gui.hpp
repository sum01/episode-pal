#ifndef EP_MAIN_GUI_HPP
#define EP_MAIN_GUI_HPP

#include "common/media.hpp"
#include "search_popover.hpp"
#include "data_view/view.hpp"
#include "config.hpp"

#include <memory>
#include <unordered_map>

#include <gtkmm-3.0/gtkmm/window.h>
#include <gtkmm-3.0/gtkmm/builder.h>
#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/searchentry.h>
#include <gtkmm-3.0/gtkmm/listbox.h>
#include <gtkmm-3.0/gtkmm/listboxrow.h>
#include <gtkmm-3.0/gtkmm/popover.h>
#include <gtkmm-3.0/gtkmm/imagemenuitem.h>
#include <gtkmm-3.0/gtkmm/checkmenuitem.h>
// For GdkEventKey
#include <gtk-3.0/gdk/gdk.h>

namespace ep::gui {
	class main_gui {
public:
		// Handles the main execution of the program.
		// Ties all the libs together into a singular gui element.
		explicit main_gui();
		// Return the main window needed to run the program.
		// The window will be initialized upon construction.
		Gtk::Window &get_window() const noexcept;
private:
		ep::config conf;
		// Handles deleting of a row
		void delete_selected() noexcept;
		// Handles making a new (empty) row
		void new_row(const ep::media::data &) noexcept;
		void move_row(const ep::gui::move) noexcept;

		// Has to be RefPtr because Glib::Builder::create returns one.
		Glib::RefPtr<Gtk::Builder> builder;
		std::unique_ptr<Gtk::Button> anime_button,
																 manga_button,
																 tv_button,
																 movie_button,
																 update_row_button,
																 new_row_button;
		std::unique_ptr<Gtk::ImageMenuItem> save_item,
																				save_as_item,
																				new_item,
																				delete_row_item,
																				open_item;
		std::unique_ptr<Gtk::CheckMenuItem> hide_description_item;

		std::unique_ptr<Gtk::Popover> settings_popover;

		// Manages the search popover
		// Holds widgets, constructs the data in a pleasing way, etc.
		// Has to be unique_ptr since we need to delay construction
		std::unique_ptr<ep::gui::search_popover> search_popover;

		std::unique_ptr<Gtk::SearchEntry> search_entry;

		std::unique_ptr<Gtk::Window> main_window;

		std::unique_ptr<ep::gui::data_view> tree_view;

		// Meta on_click for each of the anime/manga/tv/movie buttons.
		// Actually tries to call API for info, then save & display it.
		void on_click(const ep::media::type &) noexcept;

		// When the button is clicked, do something
		// anime/manga/tv/movie searches and attempts to fill the popover with info
		void on_anime_button_clicked() noexcept;
		void on_manga_button_clicked() noexcept;
		void on_tv_button_clicked() noexcept;
		void on_new_button_clicked() noexcept;
		void on_movie_button_clicked() noexcept;
		// Some self-explanitory signals
		void on_update_row_button_clicked() noexcept;
		void on_hide_description_item_clicked() noexcept;

		// Handles when the user selects some data in the search popover
		void on_search_popover_listbox_clicked(const Gtk::ListBoxRow *) noexcept;

		// Used to capture key-presses on the main window
		// FIXME: Seem to be missing the header that Gtkmm says has their version of EventKey
		// which should be in <gdkmm/events.h> https://developer.gnome.org/gtkmm/unstable/classGdk_1_1EventKey.html#details
		bool on_key_released(GdkEventKey *) noexcept;

		// Saves an ep::media::data to a vector as Gtk widgets
		// Handles putting the widgets in the order we want to display them.
		/* void save_data_fields(const ep::media::data &) noexcept; */
	};
}

#endif
