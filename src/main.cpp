#include "gui/main_gui.hpp"

#include <gtkmm-3.0/gtkmm/application.h>
// Needed for gettext
#include <glibmm-2.4/glibmm/i18n.h>
// Needed for set_application_name & set_prgname
#include <glibmm-2.4/glibmm/miscutils.h>

int main(int argc, char **argv) {
	// Putting set_application_name in main since its only supposed to be called once!
	// It must be after the call to Gtk::Application::create
	auto gtk_app = Gtk::Application::create(argc, argv);

	// The localized display name for the user. It'll show in the menubar/titlebar of the app.
	Glib::set_application_name(gettext("Episode Pal"));
	// Manages our GUI windows
	// Loads Glade files
	// etc.
	ep::gui::main_gui gui;
	// Actually launches the program using our main window
	return gtk_app->run(gui.get_window());
}
