# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](https://semver.org/).

## [Unreleased]

> [unreleased]: https://gitlab.com/sum01/episode-pal/compare/v1.0.0...HEAD
