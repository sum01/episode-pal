# Episode Pal

A simple desktop [GTK] program to help keep track of all the media you consume.

Currently anime, manga, and TV shows are supported.

# Table of Contents

1. [Dependencies](#dependencies)
   1. [Build-time](#build time)
   2. [Run-time](#run time)
2. [Build & Install](#build-install)
   1. [Cmake Options](#cmake-options)
   2. [Packaging](#packaging)
3. [Configuration](#configuration)
4. [Keybindings](#keybindings)
5. [API's used](#apis-used)
6. [Contributing](#contributing)

## Language Support

At the moment, it only supports English, but [if you wish to help with translations, read this.]

## Dependencies

### Build-time

- [Cmake] v3.12 (or newer)
- [PkgConf]
- [Gettext]

A C++20 compatible compiler is also needed.

### Run-time

| Dependency Name | Min. Version |                                                             Package(s)                                                              |
| --------------- | :----------: | :---------------------------------------------------------------------------------------------------------------------------------: |
| [cpp-httplib]   |   `0.7.1`    |   [![Packaging status](https://repology.org/badge/tiny-repos/cpp-httplib.svg)](https://repology.org/project/cpp-httplib/versions)   |
| [gtkmm]         |    `3.22`    |         [![Packaging status](https://repology.org/badge/tiny-repos/gtkmm.svg)](https://repology.org/project/gtkmm/versions)         |
| [nlohmann-json] |     `3`      | [![Packaging status](https://repology.org/badge/tiny-repos/nlohmann-json.svg)](https://repology.org/project/nlohmann-json/versions) |
| [SQLiteCpp]     |     `3`      |     [![Packaging status](https://repology.org/badge/tiny-repos/sqlitecpp.svg)](https://repology.org/project/sqlitecpp/versions)     |

[cpp-httplib] needs HTTPS & compression support, which requires [OpenSSL] and [ZLIB].

## Build & Install

Note that if you don't want to install, remove `--target install` from the last command.  
If you do that, you can also run as non-privledged (i.e. no `sudo` or `runas`)

Linux/macOS:

```
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF ..
sudo cmake --build . --target install
```

Windows:

```
mkdir build
cd build
cmake -DBUILD_TESTING=OFF ..
runas /user:Administrator "cmake --build . --config Release --target install"
```

### Cmake Options

1. `EP_INSTALL_CPPHTTPLIB` (default OFF) to install the [cpp-httplib] library. Useful for Cpack.
2. `EP_INSTALL_SQLITECPP` (default OFF) to install the [SQLiteCpp] library. Useful for Cpack.

### Packaging

There is [Cpack] support for easy packaging of the project into various package types (`.deb`, `.rpm`, etc.) or for Windows GUI installers like [NSIS] or [Wix].

After building the project, run the cpack command relevant to whichever package type you want to create.  
So for a `.deb` file, you'd run `cpack -G DEB -C Release`.

See `cpack --help` for the list of supported generators (for the `-G` option).

## Configuration

You can optionally create a `config.json` in your [XDG] config path (normally `~/.config` on Linux, or `%LOCALAPPDATA%` on Windows).  
So as an example, the full path might look like `~/.config/episode_pal/config.json` on Linux.  
This file uses regular JSON format.

Comments in this config file are supported if you built the project with a version of [nlohmann-json] >= 3.9, otherwise they won't work.

Example `config.json` file:

```json
{
	"CTRL+z": "u",
	"CTRL+r": "r",
	"omdb_key": "some_key_here"
}
```

This example rebinds `CTRL+z` to the `u` key, `CTRL+r` to the `r` key, and sets your [OMDB] api key (since we don't provide one in the project).

## Keybindings

| Default keybind |             Action performed              |
| :-------------: | :---------------------------------------: |
|    `ALT+Up`     |  Moves the selected row up one position.  |
|   `ALT+Down`    | Moves the selected row down one position. |
|    `CTRL+z`     |      Undo anything previously done.       |
|    `CTRL+r`     |     Redo anything previously undone.      |
|    `Delete`     |         Delete the selected row.          |

If you wish to rebind any of these actions, just use the same words (with the same case) as the default in your JSON.  
So for example, to rebind `ALT+Up` to `CTRL+p` you would add `"ALT+Up": "CTRL+p"` to your `config.json`.

## API's used

[Anilist] is used to find anime & manga data.  
[TVMaze] is used to find TV show data.  
[OMDB] is used to find movie data.

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md)

<!-- Hidding links for easier-to-read README -->

[anilist]: https://anilist.co/
[cmake]: https://cmake.org/
[cpp-httplib]: https://github.com/yhirose/cpp-httplib
[gtk]: https://www.gtk.org/
[gtkmm]: https://www.gtkmm.org/en/
[nlohmann-json]: https://github.com/nlohmann/json
[openssl]: https://www.openssl.org/
[pkgconf]: https://github.com/pkgconf/pkgconf
[sqlitecpp]: https://github.com/SRombauts/SQLiteCpp
[tvmaze]: https://www.tvmaze.com/
[zlib]: https://www.zlib.net/
[gettext]: https://www.gnu.org/software/gettext/
[if you wish to help with translations, read this.]: ./CONTRIBUTING.md#translations-for-translators
[cpack]: https://cmake.org/cmake/help/latest/module/CPack.html
[nsis]: https://en.wikipedia.org/wiki/Nullsoft_Scriptable_Install_System
[wix]: https://en.wikipedia.org/wiki/WiX
[omdb]: https://www.omdbapi.com/
[xdg]: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
