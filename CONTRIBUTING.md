# Contributing

## Code Style

Please use [editorconfig] for everything, [Prettier] for markdown files, and [Uncrustify] on your code before submitting any pull requests.  
I would prefer the code keeps a consistent style, and these tools do just that.

## GUI development

Want to help with GUI development?  
This project uses [Glade] for most of the GUI design. [Here's some tutorials on Glade.]

Just note that not 100% of GUI development can be done in it, and some must be done in code.

## Translations for developers

GNU's [gettext] is used for translations.  
[Here's a tutorial on its use with Gtkmm], should you need to write any strings in the code.

It basically boils down to doing `#include <glibmm/i18n.h>` wrapping strings in `gettext("example string")`, and adding the filename to [POTFILES.in] if it's not there already.  
Then you run the following in a terminal to update all translation files with the new strings.  
Note this is a [Bash] command and requires you have [intltool] installed. You might need to do something differently if not on Linux.

```bash
# assuming you've cd'd into the "po" directory
intltool-update -p -g episode_pal \
&& intltool-update -s -g episode_pal \
&& for _pofile in $(find . -regextype posix-extended -regex '.+\.po$'); do intltool-update $(basename -s .po $_pofile) -g episode_pal; done
```

## Translations for translators

Want to help with translations? GNU's [gettext] is used to easily allow for this.

Here's the list of [language codes] for reference.  
[Here's some more reading] on the specifics of the [episode_pal.pot]  
Some (optional) programs to edit `.po` files are [Lokalize], [GNOME translation editor], and [Emacs PO mode].

**Here's the steps to getting started**, using German as an example (which has a `de` [language code]).  
You can substitute `de` for whatever [language code] you're translating for.

1. If you're not on Linux: copy the [episode_pal.pot] file and call it `de.po` (putting it in the [po] folder).  
   Alternatively, if you're on Linux: `cd` into the [po] directory, and use `msginit -i episode_pal.pot`
2. Add the name of the file (without extension), so `de` in this case, to the [LINGUAS] file, making sure to put it on a new line.
3. Start editing the `de.po` file...
   1. Fill in the placeholders at the top.  
      Use `UTF-8` for the `CHARSET` (unless you're sure you need a different charset) if it isn't already filled in.
   2. Put your translations inside the `msgstr` fields, like `msgstr "Guten tag"`.  
      If you see anything like `%1` in the string, don't delete it. It needs to be there, and will be replaced with data when the program is running.
4. Submit your pull request to [the Gitlab repo].  
   If you're unsure of how to do this, send me an email at <incoming+sum01-episode-pal-18165291-issue-@incoming.gitlab.com>, or try [reading a tutorial].

<!-- Hidding links for easier-to-read README -->

[gettext]: https://www.gnu.org/software/gettext/
[editorconfig]: https://editorconfig.org/
[uncrustify]: https://github.com/uncrustify/uncrustify
[language code]: https://www.gnu.org/software/gettext/manual/html_node/Usual-Language-Codes.html
[language codes]: https://www.gnu.org/software/gettext/manual/html_node/Usual-Language-Codes.html
[the gitlab repo]: https://gitlab.com/sum01/episode-pal
[here's a tutorial on its use with gtkmm]: https://developer.gnome.org/gtkmm-tutorial/stable/chapter-internationalization.html.en
[episode_pal.pot]: ./po/episode_pal.pot
[po]: ./po
[linguas]: ./po/LINGUAS
[here's some more reading]: https://www.gnu.org/software/gettext/manual/gettext.html
[potfiles.in]: ./po/POTFILES.in
[reading a tutorial]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[glade]: https://glade.gnome.org/
[here's some tutorials on glade.]: https://wiki.gnome.org/Apps/Glade/Tutorials
[prettier]: https://github.com/prettier/prettier
[bash]: https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[intltool]: https://launchpad.net/intltool
[lokalize]: https://kde.org/applications/en/development/org.kde.lokalize
[gnome translation editor]: https://wiki.gnome.org/Apps/Gtranslator
[emacs po mode]: https://www.emacswiki.org/emacs/PoMode
