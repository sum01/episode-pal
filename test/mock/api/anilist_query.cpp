#include "api/handler.hpp"

#include <iostream>
#include <cstdlib>

void dump_err(const nlohmann::json &json) {
	if (json.empty()) {
		std::cerr << "The response JSON was empty." << std::endl;
	}
	std::cerr << "The response JSON was bad. Got:\n\t" << json.dump() << std::endl;
}

int main() {
	const auto json_response = ep::api::handler::query("attack on titan", ep::media::type::ANIME);

	// std::cout << "Got the following JSON back:\n\t" << json_response.dump() << std::endl;

	/* Expect a result with something like this in it...
				 {
				"data": {
					"Media": {
						"title": {
							"romaji": "Shingeki no Kyojin",
							"native": "進撃の巨人",
							"english": "Attack on Titan"
						},
						"type": "ANIME",
						"status": "FINISHED",
						"episodes": 25
					}
				}
			}
	 */

	const auto json_iter = json_response.find("data");

	if (json_response.empty() || json_iter == json_response.cend()) {
		dump_err(json_response);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
