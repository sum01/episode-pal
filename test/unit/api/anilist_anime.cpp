#include "api/spec/anilist.hpp"

#include <iostream>
#include <string>
#include <cstdlib>
#include <iomanip>

int main() {
	const std::string correct_body = "{ \"query\": \"query($name: String) { Page(page: 1) { media(search: $name, type:ANIME) { title { romaji english } id episodes description } } }\", \"variables\": { \"name\":\"attack on titan\" }}";

	ep::api::request correct_req;
	correct_req.body = correct_body;
	correct_req.url = "graphql.anilist.co";

	const auto res = ep::api::spec::anilist::get_request("attack on titan", ep::media::type::ANIME);

	if (res.url != correct_req.url || res.body != correct_req.body) {
		std::cerr << "Anilist anime query is malformed! Should be:\n\t"
							<< std::quoted(correct_req.url + "\n\t" + correct_req.body)
							<< "\nbut got:\n\t" << std::quoted(res.url + "\n\t" + res.body)
							<< std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
