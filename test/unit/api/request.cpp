#include "api/request.hpp"

int main() {
	// Just making sure that if something changes, this test will fail
	ep::api::request req;
	req.body = "whatever";
	req.url = "who cares";
	req.endpoint = "/wowee";
	req.type = "text/html";
	req.use_compression = false;
	req.use_post = true;

	return 0;
}
