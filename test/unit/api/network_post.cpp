#include "api/network.hpp"

#include <iostream>
#include <iomanip>
#include <cstdlib>

#include <nlohmann/json.hpp>

// Not try-catching anything
// Since we want all throws to cause failures in test
int main() {
	const ep::api::request req = {
		// postman-echo is decently reliable & easy way to echo a GET or POST request
		.url = "postman-echo.com",
		.endpoint = "/post",
		.body = "this should be echo'd",
		.type = "application/json",
		.use_post = true,
		.use_compression = false
	};

	const auto json_response = ep::api::network::get_data(req);
	const auto json_iter = json_response.find("data");
	if (json_iter == json_response.cend() || *json_iter != req.body) {
		std::cerr << "JSON response is invalid! Got:\n\t" << std::quoted(json_response.dump()) << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
