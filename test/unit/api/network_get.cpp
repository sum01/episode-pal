#include "api/network.hpp"

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>

#include <nlohmann/json.hpp>

// Not try-catching anything
// Since we want all throws to cause failures in test
int main() {
	const std::string req_data = "\"this should be echo'd\"";

	const ep::api::request req = {
		// postman-echo is decently reliable & easy way to echo a GET or POST request
		.url = "postman-echo.com",
		.endpoint = "/get?data=" + req_data,
		.body = "",
		.type = "",
		.use_post = false,
		.use_compression = false
	};

	const auto json_response = ep::api::network::get_data(req);

	if (json_response.empty()) {
		std::cerr << "JSON response is empty! Got:\n\t" << std::quoted(json_response.dump()) << std::endl;
		return EXIT_FAILURE;
	}

	const auto json_data = json_response["args"]["data"];

	if (json_data.empty()) {
		std::cerr << "JSON response data field is empty! Got:\n\t" << std::quoted(json_response.dump()) << std::endl;
		return EXIT_FAILURE;
	} else if (json_data != req_data) {
		std::cerr << "Echoed JSON response data field does not match what we sent! Got:\n\t" << std::quoted(json_response.dump()) << std::endl;
		std::cerr << "nlohmann-json found \"data\":\n\t" << std::quoted(json_data.dump()) << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
