# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-04 22:16-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/api/handler.cpp:59
msgid ""
"Unknown exception thrown trying to get the top data of a JSON query for %1 "
"with JSON \"%2\""
msgstr ""

#. TODO: maybe rethink this. idk
#: ../src/api/handler.cpp:90
msgid ""
"Unknown exception thrown trying to post-process data for %1 with name \"%2\""
msgstr ""

#: ../src/api/network.cpp:43
msgid "No response"
msgstr ""

#: ../src/api/network.cpp:45
msgid "Empty response"
msgstr ""

#: ../src/api/spec/anilist.cpp:97
msgid "The request for data from Anilist has bad/missing type or ID."
msgstr ""

#: ../src/api/spec/anilist.cpp:111
msgid "Unknown type given to Anilist's get_request call!"
msgstr ""

#: ../src/api/spec/anilist.cpp:129
msgid "Anilist was asked to get data from malformed/unexpected JSON."
msgstr ""

#: ../src/api/spec/tvmaze.cpp:50
msgid "TVMaze was asked to for a request using data without any ID!"
msgstr ""

#: ../src/api/spec/tvmaze.cpp:64 ../src/api/spec/tvmaze.cpp:259
msgid "TVMaze was given invalid JSON (not an object)."
msgstr ""

#: ../src/common/media.cpp:22
msgid "manga"
msgstr ""

#: ../src/common/media.cpp:24
msgid "TV"
msgstr ""

#: ../src/common/media.cpp:26
msgid "movie"
msgstr ""

#: ../src/common/media.cpp:30
msgid "anime"
msgstr ""

#: ../src/common/media.cpp:37
msgid "abandoned"
msgstr ""

#: ../src/common/media.cpp:39
msgid "finished"
msgstr ""

#: ../src/common/media.cpp:43
msgid "unfinished"
msgstr ""

#. Putting this here so we only call gettext on the string once
#: ../src/gui/config.cpp:24
msgid ""
"Failed to parse your config file (invalid JSON). Falling back to the program "
"defaults."
msgstr ""

#: ../src/gui/config.cpp:49
msgid "Didn't find a config file, using program defaults."
msgstr ""

#. A simple logged message if we found their OMDB API key
#: ../src/gui/config.cpp:59
msgid "Loaded OMDB API key \"%1\""
msgstr ""

#. A little success message when we loaded their custom keybinding. %1 will be their custom keybind, %2 will be the actual corresponding keybind.
#: ../src/gui/config.cpp:86
msgid "Using your custom keybinding \"%1\" for keybinding \"%2\""
msgstr ""

#. A little failure message when we failed to load their custom keybinding. %1 will be their custom keybind, %2 will be the actual corresponding keybind.
#: ../src/gui/config.cpp:89
msgid "Failed to use your custom keybinding \"%1\" for keybinding \"%2\"!"
msgstr ""

#: ../src/gui/config.cpp:132
msgid "Invalid keyval used in your custom keybinding with value \"%1\""
msgstr ""

#. Means they used an invalid modifier.
#. Report the error.
#: ../src/gui/config.cpp:153
msgid "Invalid modifier used in your custom keybinding with value \"%1\""
msgstr ""

#. The %1 will be filled with a path to a file we loaded
#: ../src/gui/glade_loader.cpp:19
msgid "Successfully loaded Glade file %1"
msgstr ""

#. The %1 will be filled with the path we unsuccessfully attempted to load.
#: ../src/gui/glade_loader.cpp:66
msgid "Failed to find the desired Glade file %1"
msgstr ""

#. Simple error message. The %1 is replaced with the ID of the object and should not be changed!
#: ../src/gui/helper.hpp:34
msgid "Failed to load the target object \"%1\""
msgstr ""

#.
#. The %1 is replaced with the ID of the widget.
#. So for example, it would be "Failed to load the target widget "search_entry""
#.
#: ../src/gui/helper.hpp:55 ../src/gui/helper.hpp:79
msgid "Failed to load the target widget \"%1\""
msgstr ""

#.
#. A generic failure message for if our request to the API gave us nothing back.
#. This doesn't mean it actually failed though, just returned nothing.
#. The %1 will be substituted with the string of the media type (so Anime/Manga/TV/Movie/etc.)
#. So for example, it'll be "The anime API returned nothing when we requested data!"
#.
#: ../src/gui/main_gui.cpp:108
msgid "The %1 API returned nothing when we requested data!"
msgstr ""

#.
#. The %1 will be substituted with the string of the media type (so Anime/Manga/TV/Movie/etc.)
#. So for example, it'll be "Failed when trying to query the anime API!"
#.
#: ../src/gui/main_gui.cpp:126
msgid "Failed when trying to query the %1 API for data!"
msgstr ""

#: ../src/gui/search_popover.cpp:67
msgid "Tried to get data from the search popover when there was none!"
msgstr ""

#. If this triggered, it's because there was some odd issue where GTK returned nothing...
#. when we tried to get the selected row.
#: ../src/gui/search_popover.cpp:78
msgid ""
"Tried to get the selected row's widget in the search popover, but there was "
"none!"
msgstr ""

#. This should probably never trigger, and is mostly just here so code compiles.
#: ../src/gui/search_popover.cpp:90
msgid ""
"Failed to find relevant data from the selected row in the search popover!"
msgstr ""

#: ../src/gui/tree_view.cpp:231
msgid "TreeViewColumn was unexpectedly null (tree_view::insert_data)."
msgstr ""

#: ../src/gui/tree_view.cpp:245
msgid "CellRenderer was unexpectedly null (tree_view::insert_data)."
msgstr ""

#: ../src/gui/tree_view.cpp:454
msgid ""
"TreeSelection was unexpectedly null. Returning the end iterator instead "
"(tree_view::get_selected)."
msgstr ""

#: ../src/gui/tree_view.cpp:478
msgid ""
"The TreeViewColumn was unexpectedly null (tree_view::set_column_visibile)."
msgstr ""

#. The localized display name for the user. It'll show in the menubar/titlebar of the app.
#: ../src/main.cpp:15
msgid "Episode Pal"
msgstr ""
