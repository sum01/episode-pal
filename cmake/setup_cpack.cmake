# This function is a kind of meta-command to properly scope the various Cpack variables.
# Using PARENT_SCOPE so the subdirs that call this propogate the value into the scope of the caller.
# Does not append duplicate strings. If this happens, it simply exits the function.
function(ep_scope_append _var_to_append _append_value _optional_delimiter)
	# If the variable we're appending to is empty, just put the value into it.
	# then exit.
	if(NOT ${_var_to_append})
		set(${_var_to_append} "${_append_value}" PARENT_SCOPE)
		return()
	else()
		# Prevent adding duplicate values.
		string(FIND "${{_var_to_append}}" "${_append_value}" _substring_index)
		# It equals -1 if the substring is not found.
		# So != -1 means it was found, and we should stop here.
		if (NOT _substring_index EQUAL -1)
			# Exit since we found a duplicate.
			return()
		endif()
	endif()

	string(LENGTH ${_optional_delimiter} _len)
	if(_len LESS_EQUAL 1)
		# Adds a space if passed a single-char delimiter.
		# Otherwise just adds a space to the previously-empty variable.
		set(_optional_delimiter "${_optional_delimiter} ")
	endif()
	# Double-expand to first get var name, then its value
	set(${_var_to_append} "${${_var_to_append}}${_optional_delimiter}${_append_value}" PARENT_SCOPE)
endfunction()
# Various helpers to reduce cruft versus directly calling ep_scope_append.
function(ep_append_debian_dep _dep_str)
	# Note that this will only need to be the debian packages that are libraries we linked.
	# So header-only things like Nlohmann-json don't need to be added.
	# Things that don't have packages will need to be put inside the .deb and their dependencies added here!
	ep_scope_append("CPACK_DEBIAN_PACKAGE_DEPENDS" "${_dep_str}" "")
endfunction()
function(ep_append_rpm_dep _dep_str)
	# The Cpack RPM var requires(?) you use a comma to delimit all the strings
	ep_scope_append("CPACK_RPM_PACKAGE_REQUIRES" "${_dep_str}" ",")
endfunction()
function(ep_append_debian_provides _provides_str)
	ep_scope_append("CPACK_DEBIAN_PACKAGE_PROVIDES" "${_provides_str}" "")
endfunction()
function(ep_append_rpm_provides _provides_str)
	ep_scope_append("CPACK_RPM_PACKAGE_PROVIDES" "${_provides_str}" "")
endfunction()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Recommended by Cmake docs to include this. I guess it's for Windows mainly?
include(InstallRequiredSystemLibraries)
# Puts the license in the installer GUI for the user to see
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README.md")
# Strip the binaries for smaller files
set(CPACK_STRIP_FILES TRUE)
# Creates a checksum file in the package, using SHA512.
# See list of algo's here https://cmake.org/cmake/help/latest/command/string.html#supported-hash-algorithms
# The file will be named "episode_pal.SHA512"
set(CPACK_PACKAGE_CHECKSUM SHA512)
# ~~~~ Vars for various generators ~~~~
set(CPACK_PACKAGE_DESCRIPTION "${PROJECT_DESCRIPTION}")
set(CPACK_PACKAGE_CONTACT "sum01 <sum01@protonmail.com>")
set(CPACK_DEBIAN_PACKAGE_HOMEPAGE "${PROJECT_HOMEPAGE_URL}")

# Option and functions declared in main scope
if(EP_INSTALL_CPPHTTPLIB)
	ep_append_debian_provides(cpp-httplib=${httplib_VERSION})
	ep_append_rpm_provides(cpp-httplib=${httplib_VERSION})
endif()
# if(EP_INSTALL_SQLITECPP)
# 	ep_append_debian_provides("SQLiteCpp=${SQLiteCpp_VERSION}")
# 	# TODO: remove this when they get v3 in their repo's. Right now it's v2 (as of 2020/7/06).
# 	ep_append_rpm_provides("sqlitecpp=${SQLiteCpp_VERSION}")
# endif()

ep_append_debian_dep("openssl>=${_httplib_openssl_minver}")
ep_append_debian_dep("libgtkmm-3.0-1v5>=${_gtkmm_minver}")
ep_append_debian_dep("zlib1g>=${_httplib_zlib_minver}")
# ep_append_debian_dep("sqlite3>=${_sqlite_minver}")

ep_append_rpm_dep("openssl >= ${_httplib_openssl_minver}")
ep_append_rpm_dep("libgtkmm-3_0-1 >= ${_gtkmm_minver}")
ep_append_rpm_dep("zlib >= ${_httplib_zlib_minver}")
# TODO: add "sqlitecpp" when they get v3 in their repo's. Right now it's v2 (as of 2020/7/06).
# ep_append_rpm_dep("sqlite >= ${_sqlite_minver}")

# Must be included after all the cpack variables are set.
# These functions come from the parent scope
include(CPack)
