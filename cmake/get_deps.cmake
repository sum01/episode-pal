# TODO: maybe use CPM to (optionally) download & build deps the user doesn't have...
# see https://github.com/iauns/cpm

# Used for translating text in code using the .po and .glade files
# Used by most/all libs so doing it in main scope
find_package(Gettext REQUIRED)
# Generates all the translations into binary files
GETTEXT_PROCESS_POT_FILE("po/POTFILES.in" ALL
	INSTALL_DESTINATION "${CMAKE_INSTALL_LOCALEDIR}/${PROJECT_NAME}"
)

find_package(nlohmann_json ${_nlohmann_json_minver} REQUIRED)

find_package(PkgConfig REQUIRED)
# Finds gtkmm
# Makes an IMPORTED target PkgConfig::gtkmm3
pkg_check_modules(gtkmm REQUIRED IMPORTED_TARGET gtkmm-3.0>=${_gtkmm_minver})

# See https://github.com/yhirose/cpp-httplib
# Creates httplib::httplib
find_package(httplib ${_cpp_httplib_minver} REQUIRED)
if(NOT HTTPLIB_IS_USING_OPENSSL)
	message(FATAL_ERROR "The found cpp-httplib doesn't have OpenSSL support, but ${PROJECT_NAME} requires it.")
endif()
# TODO: This might not be needed, depending on what API's we use, and if any allow compressing requests.
if(NOT HTTPLIB_IS_USING_ZLIB)
	message(FATAL_ERROR "The found cpp-httplib doesn't have ZLIB support, but ${PROJECT_NAME} requires it.")
endif()

# find_package(SQLiteCpp ${_sqlitecpp_minver} REQUIRED)
